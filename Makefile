SRC = src
SOURCES_GCC = $(notdir $(wildcard $(SRC)/*.c))
SOURCES_CXX = $(notdir $(wildcard $(SRC)/*.cpp))
SOURCES_AS  = $(notdir $(wildcard $(SRC)/*.S))
OBJECTS_GCC = $(SOURCES_GCC:.c=.o)
OBJECTS_CXX = $(SOURCES_CXX:.cpp=.o)
OBJECTS_AS  = $(SOURCES_AS:.S=.o)
INCLUDE = include
LIBS = 
OUTPUT = code.bin
LDFLAGS = --no-demangle
LOCATION = 0x80234560
LDSCRIPT  = pal.x
PAL_LDSCRIPT  = pal.x
NTSC_LDSCRIPT = ntsc.x
PAL_MAPFILE  = pal.map
NTSC_MAPFILE = ntsc.map

CC      = powerpc-eabi-gcc
CXX     = powerpc-eabi-g++
AS      = powerpc-eabi-as
LD      = powerpc-eabi-ld
STRIP   = powerpc-eabi-strip
OBJCOPY = powerpc-eabi-objcopy

all:
	@make -s pal
	@make -s ntsc
	@make -s loader

%.o: src/%.cpp
	@echo "Compiling $<..."
	@$(CXX) -nodefaultlibs -I. -I $(INCLUDE) -fno-builtin -Os -fno-exceptions -fno-rtti -mno-sdata -c -o $@ $<

%.o: src/%.S
	@echo "Assembling $<..."
	@$(CXX) -nodefaultlibs -I. -I $(INCLUDE) -fno-builtin -Os -fno-exceptions -fno-rtti -mno-sdata -c -o $@ $<

$(OUTPUT): $(OBJECTS_CXX) $(OBJECTS_AS)
	@echo "Linking $^..."
	@$(LD) -L. -o $(OUTPUT) -Ttext $(LOCATION) -T $(LDSCRIPT) $(LDFLAGS) $(OBJECTS_CXX) $(OBJECTS_AS)

pal: $(OBJECTS_CXX) $(OBJECTS_AS)
	@echo "Creating pal..."
	@$(LD) -L. -o pal_$(OUTPUT) -Ttext $(LOCATION) -T $(PAL_LDSCRIPT) -Map=$(PAL_MAPFILE) $(LDFLAGS) $(OBJECTS_CXX) $(OBJECTS_AS)

ntsc: $(OBJECTS_CXX) $(OBJECTS_AS)
	@echo "Creating ntsc..."
	@$(LD) -L. -o ntsc_$(OUTPUT) -Ttext $(LOCATION) -T $(NTSC_LDSCRIPT) -Map=$(NTSC_MAPFILE)  $(LDFLAGS) $(OBJECTS_CXX) $(OBJECTS_AS)

loader:
	@echo "Assembling loader..."
	@$(CC) -nostartfiles -nodefaultlibs -Wl,-Ttext,0x80001800 -o tmp.elf loader.S
	@$(STRIP) --strip-debug --strip-all --discard-all -o tmps.elf -F elf32-powerpc tmp.elf
	@$(OBJCOPY) -I elf32-powerpc -O binary tmps.elf loader.bin
	@rm tmp.elf
	@rm tmps.elf

clean:
	@echo "Cleaning up..."
	@rm -f pal_$(OUTPUT) ntsc_$(OUTPUT) $(OUTPUT) $(OBJECTS_CXX) $(OBJECTS_AS) $(OBJECTS_GCC) $(PAL_MAPFILE) $(NTSC_MAPFILE) loader.bin

