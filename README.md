Creates patches for Mario Kart Wii

possible compile targets:
	loader - creates the loader.bin to be hooked in after .rel is loaded into memory
	pal    - creates the pal_code.bin that will be loaded by loader.bin
	ntsc   - creates the ntsc_code.bin that will be loaded by loader.bin
	clean  - cleans up build
	all    - creates pal, ntsc, and loader

