OUTPUT_FORMAT ("binary")

/*MEMORY {
	loader_section : ORIGIN = 0x80001800, LENGTH = 0x1800
}*/

SECTIONS {
	main     = 0x8000B6B0;
	_main    = 0x80008EF0;
	OSReport = 0x801A25D0;
	OSFatal  = 0x801A4EC4;

	.text : {
		FILL (0)

		__text_start = . ;
		*(.init)
		*(.text)
		__ctor_loc = . ;
		*(.ctors)
		__ctor_end = . ;
		*(.dtors)
		*(.rodata)
		/**(.sdata)*/
		*(.data)
		/**(.sbss)*/
		*(.bss)
		*(.fini)
		*(.rodata.*)
		__text_end  = . ;
	}
}
