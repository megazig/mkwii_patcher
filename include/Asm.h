#ifndef __ASM_H__
#define __ASM_H__

.set r0,0;   .set r1,1;   .set r2,2; .set r3,3;   .set r4,4
.set r5,5;   .set r6,6;   .set r7,7;   .set r8,8;   .set r9,9
.set r10,10; .set r11,11; .set r12,12; .set r13,13; .set r14,14
.set r15,15; .set r16,16; .set r17,17; .set r18,18; .set r19,19
.set r20,20; .set r21,21; .set r22,22; .set r23,23; .set r24,24
.set r25,25; .set r26,26; .set r27,27; .set r28,28; .set r29,29
.set r30,30; .set r31,31; .set f0,0; .set f2,2; .set f3,3
.set sp,1;

.macro  GOTO address
	lis r12, \address@h
	ori r12, r12, \address@l
	mtctr r12
	bctr
.endm

.macro  CALL function
	lis r12, \function@h
	ori r12, r12, \function@l
	mtctr r12
	bctrl
.endm

/*Absolute branch if equal*/
.macro  abeq address
	lis r12, \address@h
	ori r12, r12, \address@l
	mtctr r12
	beqctr-
.endm

.macro  lwi reg, value
	lis \reg, \value@h
	ori \reg, \reg, \value@l
.endm


.macro lfsi freg, value
	.if \value == 0
		fsubs \freg, \freg, \freg
	.else
		bl 0f
		.float \value
		0:
		mflr r12
		lfs \freg, 0(r12)
	.endif
.endm

.macro fsqrt freg1, freg2
	frsqrte \freg1, \freg2
	fres \freg1, \freg1
.endm

.macro  stmfd from, to, offset, reg
	stfd \from, \offset(\reg)
	.if \to-\from
		stmfd "(\from+1)", \to, \offset+8, \reg
	.endif
.endm

.macro  lmfd from, to, offset, reg
	lfd \from, \offset(\reg)
	.if \to-\from
		lmfd "(\from+1)", \to, \offset+8, \reg
	.endif
.endm

.macro  PATCH reg1, reg2, address, value
	lis \reg1, \address@ha
	lis \reg2, \value@h
	ori \reg2, \reg2, \value@l
	stw \reg2, \address@l(\reg1)
.endm

#endif
