#ifndef __NINTENDO_H__
#define __NINTENDO_H__

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;

#ifdef __cplusplus
   extern "C" {
#endif /* __cplusplus */

int main(int argc, char ** argv);
int _main(int argc, char ** argv);
void memcpy(void* dst, void* src, int length);

//void OSPanic(const char* format, ...);
void OSReport(const char* format, ...);
void OSFatal(int fd, int foo, const char* format, ...);

int IOS_Open(const char* path, int mode);
int IOS_Close(int fd);
int IOS_Ioctl(int fd, int ioctl_num, void* inbuf, int inlen, void* outbuf, int outlen);
int IOS_Ioctlv(int fd, int ioctlv_num, int innum, int outnum, void* vectors);

#ifdef __cplusplus
   }
#endif /* __cplusplus */

namespace nw4r
{
	namespace math
	{
		float Hermite(float,float,float,float,float);
	}
};

#endif
