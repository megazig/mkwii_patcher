#include "Nintendo.h"

void (*pOSReport)(const char* format, ...) = &OSReport;

int print_some_crap()
{
	(*pOSReport)("Here is some debug output\n");
	(*pOSReport)("And some more\n");
	OSReport("What!?\n");
	return -25;
}

int test_foo(const char* foo, int bar, void* baz)
{
	OSReport("Holy crap\n");
	(*pOSReport)(foo, bar, baz);
	(*pOSReport)("How's about that\n");
	return 0;
}

