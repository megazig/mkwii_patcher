#ifndef BUFSTREAM_H

#define BUFSTREAM_H

#include "BinStream.h"

class BufStream : public BinStream
{
	public:
		char * buf_ptr;
		bool fail;
		int stream_current;
		int stream_length;
		void * StreamChecksum_ptr;
		int amt_read_myb;

		BufStream(void* buffer, int length, bool swap);
		virtual ~BufStream(void);
		virtual void Flush(void);
		virtual int Tell(void);
		virtual int Eof(void);
		virtual bool Fail(void);
		//virtual const char * Name(void);	//BinStream::Name()
		//virtual Cached(void);				//BinStream::Cached()
		//virtual GetPlatform(void);		//BinStream::GetPlatform()
		virtual void ReadImpl(void * buffer, int length);
		virtual void WriteImpl(void const * buffer, int length);
		virtual void SeekImpl(int where, BinStream::SeekType type);

		/* non virtual */
		void BufStream::DeleteChecksum(void);
};

BufStream::BufStream(void * buffer, int length, bool swap) : BinStream(swap)
{
	this->StreamChecksum_ptr = 0;
	this->amt_read_myb = 0;
	this->buf_ptr = buffer;
	this->fail = (buffer == 0);
	this->stream_current = 0;
	this->stream_length = length;
}

//BufStream::~BufStream(bool del)
BufStream::~BufStream(void)
{
	this->DeleteChecksum();
	/* DELETE */
}

void BufStream::DeleteChecksum(void)
{
	/* FIXME */
	if (this->StreamChecksum_ptr)
	{
		delete this->StreamChecksum_ptr;
	}
	this->StreamChecksum_ptr = 0;
	this->amt_read_myb = 0;
}

void BufStream::ReadImpl(void * buffer, int length)
{
	if (this->stream_current + length > this->stream_length)
	{
		length = this->stream_length - this->stream_current;
		this->fail = true;
	}

	memcpy(buffer, this->buf_ptr + this->stream_current, length);
	this->stream_current += length;

	if (this->StreamChecksum_ptr && !this->fail)
	{
		this->StreamChecksum_ptr->Update(buffer, length);
		this->amt_read_myb += length;
	}
}

void BufStream::WriteImpl(void const * buffer, int length)
{
	if (this->stream_current + length > this->stream_length)
	{
		length = this->stream_length - this->stream_current;
		this->fail = true;
	}

	memcpy(this->buf_ptr + this->stream_current, buffer, length);
	this->stream_current += length;
}

void BufStream::SeekImpl(int where, BinStream::SeekType whence)
{
	int result;
	if (whence == 0) // SET
	{
		result = where;
	}
	else if (whence == 1) // CURRENT
	{
		result = this->stream_current + where;
	}
	else if (whence == 2) // END
	{
		result = this->stream_length + where;
	}
	else
	{
		return;
	}

seek_set:
	if (where < 0 || result > this->stream_length)
	{
		this->fail = true;
		return;
	}
	this->stream_current = result;
}

bool BufStream::Fail(void)
{
	return this->fail;
}

bool BufStream::Eof(void)
{
	return (this->stream_length - this->stream_current == 0);
}

int BufStream::Tell(void)
{
	return this->stream_current;
}

void BufStream::Flush(void)
{
}

#endif /* end of include guard: BUFSTREAM_H */
