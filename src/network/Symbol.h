#ifndef SYMBOL_H

#define SYMBOL_H

char NullStr[1] = {0};
char * gNullStr = NullStr;

class Symbol
{
	public:
		char * string;
};

Symbol::Symbol(void) { this->string = gNullStr; }
Symbol::Symbol(const char * string)
{
	if (string == 0 || *string == 0)
	{
		this->string = gNullStr;
		return;
	}

}

#endif /* end of include guard: SYMBOL_H */
