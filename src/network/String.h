#ifndef STRING_H

#define STRING_H

char gEmpty[1];

class String : public TextStream
{
	public:
		int _field_00;
		int _field_04;
		char * str_buf;

		String(void);
		String(const char * buf);
		virtual ~String();
		virtual void Print(const char * str);
};

String::String(void) : TextStream()
{
	this->_field_04 = 0;
	this->str_buf = gEmpty;
}
String::String(const char * buf) : TextStream()
{
	/* FIXME */
}
String::String(Symbol symb) : TexStream()
{
	/* FIXME */
}
String::String(String const & str)
{
	/* FIXME */
}

char * String::c_str(void)
{
	return str_buf;
}

#endif /* end of include guard: STRING_H */
