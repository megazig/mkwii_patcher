#ifndef CSHA1_H

#define CSHA1_H

class CSHA1
{
	public:
};

CSHA1::CSHA1(void)
{
	this->_field_C0 = &this->_field_80;
	this->Reset();
}

//CSHA1::~CSHA1(bool del)
CSHA1::~CSHA1(void)
{
	if (this)
		this->Reset();
	/*
	if (del)
		delete this;
	*/
	/* DEL */
}

void CSHA1::Reset(void)
{
	this->_field_00 = 0x67452301;
	this->_field_04 = 0xEFCDAB89;
	this->_field_08 = 0x98BADCFE;
	this->_field_0C = 0x10325476;
	this->_field_10 = 0xC3D2E1F0;
	this->_field_14 = 0;
	this->_field_18 = 0;
}

void CSHA1::Transform(unsigned long * r4, unsigned const char * r5)
{
	/* FIXME */
}

void CSHA1::Update(unsigned char * r4, unsigned long r5)
{
	/* FIXME */
}

void CSHA1::Final(void)
{
	/* FIXME */
}

void CSHA1::GetHash(unsigned char * r4)
{
	memcpy(r4, &this->_field_60, 0x14);
}

HashString(char const * string, int seed)
{
	int hash = 0;
	char val;
	while (val = *string)
	{
		int r0 = hash * 0x7F;
		val += r0;
		r0 = val / seed;
		r0 = r0 * seed;
		hash = val - r0;
		string++;
	}
	return hash;
}

#endif /* end of include guard: CSHA1_H */
