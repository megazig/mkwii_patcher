#include <NetworkSocket_Wii.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
#define NULL 0
void OSReport(const char * format, ...);
void * WiiAllocHeap(int*, int);
void   WiiFreeHeap(void*, int);
void * _NetAlloc(u32,u32);
void   _NetFree(void*);

//static bool WiiNetworkSocket::sInit;

typedef struct WiiNetworkSocket_Mallocs WiiNetworkSocket_Mallocs;
struct WiiNetworkSocket_Mallocs {
	//void * alloc;
	//void * free;
	void* (*alloc)(u32, u32);
	void  (*free)(void*);
};

WiiNetworkSocket_Mallocs _mallocs = { _NetAlloc, _NetFree };

void * NetAllocFunc(int r3, int r4)
{
	if (r3 <= 0)
		return NULL;

	int * foo = &r3;
	return WiiAllocHeap(foo, 1);
}
void * NetAlloc(unsigned long r3, long r4)
{
	NetAllocFunc(r4, 0x20);
}
void NetFree(unsigned long r3, void * buffer, long r5)
{
	if (!r5 || !buffer)
		return;

	WiiFreeHeap(buffer, 1);
}


bool WiiNetworkSocket::Init(void)
{
	if (!WiiNetworkSocket::sInit)
	{
		WiiNetworkSocket_Mallocs mallocs = _mallocs;
		int ret = SOInit(mallocs);
		if (ret!=0 || ret!=-7)
		{
			return false;
		}

		ret = SOStartup();
		if (ret!=0 || ret!=-7)
		{
			return false;
		}

		WiiNetworkSocket::sInit = true;
	}
	return true;
}

void WiiNetworkSocket::Clean(void)
{
	WiiNetworkSocket::sInit = false;
}

WiiNetworkSocket::WiiNetworkSocket(bool foo)
{
	this->_field_08 = foo;
	this->fail = false;

	this->Init();

	if (!this->_field_08)
		this->socket = SOSocket(2, 2, 0);
	else
		this->socket = SOSocket(2, 1, 0);

	if (this->socket >= 0)
	{
		int ret = SOFcntl(this->socket, 3, 0);
		SOFcntl(this->socket, 4, ret | 4);
	}
}

WiiNetworkSocket::WiiNetworkSocket(int socket, bool foo)
{
	this->socket = socket;
	this->_field_08 = foo;
	this->fail = false;

	int ret = SOFcntl(socket, 3, 0);
	SOFcntl(this->socket, 4, ret | 4);
}

//WiiNetworkSocket::~WiiNetworkSocket(int del)
WiiNetworkSocket::~WiiNetworkSocket(void)
{
	if (this)
	{
		this->Disconnect();
		//if (del > 0)
		//	delete this;
	}
}

bool WiiNetworkSocket::Connect(unsigned int r4, unsigned short r5)
{
	u8 buffer[8];
	buffer[0] = 8;
	buffer[1] = 2;
	*(u16*)(buffer + 2) = r5;
	*(u32*)(buffer + 4) = r4;
	int ret = SOConnect(this->socket, buffer);
	if (ret >= 0)
		return (ret == 0);
	if (ret != -26)
	{
		this->fail = true;
		return (ret == 0);
	}
	return true;
}

void WiiNetworkSocket::Listen(void)
{
	SOListen(this->socket, 5);
}

WiiNetworkSocket * WiiNetworkSocket::Accept(void)
{
	u8 buffer[1] = {8};
	int ret = SOAccept(this->socket, buffer);
	if (ret < 0)
		return NULL;

	return new WiiNetworkSocket(ret, this->_field_08);
}

void WiiNetworkSocket::GetRemoteIP(unsigned int & r4, unsigned short & r5)
{
	OSReport("NetworkSocket_Wii.cpp, need to implement: ");
	OSReport("WiiNetworkSocket::GetRemoteIP");
	OSReport("\n");
}

void WiiNetworkSocket::Disconnect(void)
{
	if (this->socket >= 0)
	{
		SOShutdown(this->socket, 2);
		SOClose(this->socket);
		this->socket = -1;
	}
}

bool WiiNetworkSocket::Fail(void)
{
	return this->fail;
}

void WiiNetworkSocket::Bind(unsigned short r4)
{
	int buffer[1] = {1};
	SOSetSockOpt(this->socket, 0xFFFF, 4, buffer, 4);
	unsigned int ret = SOHtoNl(0);
	u8 buffer2[8];
	buffer2[0] = 8;
	buffer2[1] = 2;
	*(u16*)(buffer2 + 2) = r4;
	*(u32*)(buffer2 + 4) = ret;
	SOBind(this->socket, buffer2);
}

int WiiNetworkSocket::InqBoundPort(unsigned short & r4)
{
	OSReport("NetworkSocket_Wii.cpp, need to implement: ");
	OSReport("WiiNetworkSocket::InqBoundPort");
	OSReport("\n");
	return 0;
}

int WiiNetworkSocket::Send(void const * buffer, unsigned long size)
{
	if (this->fail)
		return 0;

	int ret = SOSend(this->socket, buffer, size, 0);
	if (ret == -15 || ret == -56)
	{
		this->fail = true;
		return 0;
	}
	if (ret == -6)
		return 0;
	if (ret >= 0)
		return ret;

	this->Disconnect();
	this->fail = true;
	return 0;
}

int WiiNetworkSocket::SendTo(void const * buffer, unsigned long size, unsigned int r6, unsigned short r7)
{
	OSReport("NetworkSocket_Wii.cpp, need to implement: ");
	OSReport("WiiNetworkSocket::SendTo");
	OSReport("\n");
	return 0;
}

int WiiNetworkSocket::BroadcastTo(void const * buffer, unsigned long size, unsigned short r6)
{
	OSReport("NetworkSocket_Wii.cpp, need to implement: ");
	OSReport("WiiNetworkSocket::BroadcastTo");
	OSReport("\n");
	return 0;
}

int CanRead(int socket)
{
	int ret = 0;

	int buffer[3] = {socket, 1, 0};
	int poll = SOPoll(buffer, 1, 0, 0);
	if (poll >= 0 && buffer[1] == buffer[2])
		ret = 1;

	return ret;
}

int WiiNetworkSocket::Recv(void * buffer, unsigned long size)
{
	if (this->fail)
		return 0;

	if (!CanRead(this->socket))
		return 0;

	int ret = SORecv(this->socket, buffer, size, 0);
	if (ret == -15 || ret == -56)
	{
		this->fail = true;
		return 0;
	}
	if (ret == -6)
		return 0;
	if (ret >= 0)
		return ret;

	return 0;
}

int WiiNetworkSocket::RecvFrom(void * buffer, unsigned long size, unsigned int & r6, unsigned short & r7)
{
	OSReport("NetworkSocket_Wii.cpp, need to implement: ");
	OSReport("WiiNetworkSocket::RecvFrom");
	OSReport("\n");
	return 0;
}

int WiiNetworkSocket::CanSend(void)
{
	int ret = 0;

	int buffer[3] = {this->socket, 1, 0};
	int poll = SOPoll(buffer, 1, 0, 0);
	if (poll >= 0 && buffer[1] == buffer[2])
		ret = 1;

	return ret;
}

bool WiiNetworkSocket::BytesAvailable(void)
{
	u8 buffer[0x5DC];
	int ret = SORecv(this->socket, buffer, 0x5DC, 2);
	return (ret > 0);
}

bool WiiNetworkSocket::SetNoDelay(bool delay)
{
	bool buffer[1] = {delay};
	int ret = SOSetSockOpt(this->socket, 6, 0x2001, buffer, 4);
	return (ret == 0);
}

