/* MAKES SHIT WORK */
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef signed int s32;
typedef signed short s16;
typedef signed char s8;
#define NULL 0

typedef void OSThread;

#include "revolution/nwc24/NWC24Err.h"

/* FORWARD DECLARATIONS */
int OSDisableInterrupts(void);
void OSRestoreInterrupts(int _intrpts);
static void PutLastError(s32 result);
static int ConvNWC24Err(NWC24Err err, s32 exErr);
void OSRegisterVersion(const char* version);
void NETMemSet(void* dst, char character, int size);
char* OSGetCurrentThread(void);
int OSRoundUp32B(int size);

void* SOiAlloc(u32 name, s32 size);
void  SOiFree(u32 name, void* ptr, s32 size);
int   SOiStartupCore(int value);
/* END FORWARD DECLARATIONS */


/* Allocator type definition */
typedef void*   ( *SOAlloc )    ( u32 name, s32 size );
typedef void    ( *SOFree )     ( u32 name, void* ptr, s32 size );

/* Config of SOInit() */
typedef struct SOLibraryConfig
{
    SOAlloc     alloc;
    SOFree      free;

} SOLibraryConfig;

int     SOInit      ( SOLibraryConfig* config );
int     SOFinish    ( void );
int     SOStartup   ( void );
int     SOStartupEx ( int timeOut );
int     SOCleanup   ( void );

/* shit structs */
//typedef struct soWork_t
typedef struct SOSysWork
{
	SOAlloc alloc;
	SOFree  free;
	int rmState;
	int rmFd;		// /dev/net/ip/top fd
	/* FIXME */
	//SOResolver* pResolver;
	void* pResolver;
	int allocCount;
//} soWork_t;
} SOSysWork;

/* r13 globals */
/* FIXME */
#define SO_INTERNAL_STATE_TERMINATED 0
#define SO_SUCCESS 0
typedef int BOOL;
#define FALSE 0
#define TRUE (!0)
/* end FIXME */

static u8 soState = SO_INTERNAL_STATE_TERMINATED;
static SOSysWork soWork;
static s32 soError = SO_SUCCESS;
static BOOL soRegistered = FALSE;
static BOOL soBufAddrCheck = TRUE;
//soWork_t soWork;

/* other crap globals */
const char* __SOVersion = "<< RVL_SDK - SO \tdebug build: Jan 19 2009 15:46:49 (0x4199_60831) >>";
//int soBufAddrCheck = 1;

int SOInit(SOLibraryConfig* config)
{
	int result = 0;	// SO_SUCCESS
	BOOL enabled = OSDisableInterrupts();
	if (!soRegistered)
	{
		OSRegisterVersion(__SOVersion);
		soRegistered = TRUE;
	}
	switch (soState)
	{
		case SO_INTERNAL_STATE_READY:
		case SO_INTERNAL_STATE_ACTIVE:
			/* Already intialized */
			SOiReport( SO_REPORT_ALREADY_COMPLETE );
#define SO_EALREADY -7
			result = SO_EALREADY;
			break;
		case SO_INTERNAL_STATE_TERMINATED:
		default:
			/* Confirms parameters */
			if ((config==NULL) || (config->alloc==NULL) || (config->free==NULL) )
			{
				SOiReport( SO_REPORT_INVAL_PARAM );
#define SO_EINVAL -28
				result = SO_EINVAL;
				break;
			}
			/* Initialize Library */
			(void)NETMemSet((void*)&soWork, 0, sizeof(SOSysWork) );
			soWork.alloc = config->alloc;
			soWork.free  = config->free;
			soWork.allocCount = 0; // allocCount
#define SO_INTERNAL_RM_STATE_CLOSED -2
			soWork.rmState = SO_INTERNAL_RM_STATE_CLOSED; // rmState -2
			soWork.rmFd = -1; //  rmFd
#define SO_MEM_RESOLVER 0xB
/* FIXME OSRoundUp32B( sizeof(SOResolver) ) */
			//soWork.pResolver = (SOResolver*)SOiAlloc(SO_MEM_RESOLVER, 0x460);
			soWork.pResolver = SOiAlloc(SO_MEM_RESOLVER, 0x460);
			if (!soWork.pResolver)
			{
#define SO_ENOMEM -49
				result = SO_ENOMEM;
				SOiErrorReport(result);
				break;
			}
#define SO_INTERNAL_STATE_READY 1
			soState = SO_INTERNAL_STATE_READY;
	}

	PutLastError((s32)result);
	(void)OSRestoreInterrupts(enabled);
	return result;
}

int SOFinish(void)
{
	int result = 0;		// SO_SUCCESS
	BOOL enabled = OSDisableInterrupts();

	switch (soState)
	{
		case SO_INTERNAL_STATE_TERMINATED:
			/* Already shut down */
			SOiReport(SO_REPORT_ALREADY_COMPLETE);
			result = SO_EALREADY;
			break;
#define SO_INTERNAL_STATE_READY 1
		case SO_INTERNAL_STATE_READY:
			if( soWork.rmState > SO_INTERNAL_RM_STATE_CLOSED )
			{
				/* Currently using resource manager */
				SOiReport( SO_REPORT_WORKING );
#define SO_EBUSY -10
				result  = SO_EBUSY;
				break;
			}
			else if( soWork.allocCount > 1 )
			{
				/*
				 * There is still dynamically allocated memory that has not been released.
				 * If the priority of the thread that calls SOCleanup and SOFinish is high, there is a possibility that control will reach this point before dynamically allocated memory is released, when resources do not reach threads that call functions like SORead that were being blocked.
				 */
				SOiReport( SO_REPORT_MEMLEAK );
#define SO_EAGAIN -6
				result  = SO_EAGAIN;
				break;
			}
			/* shut down library */
			soState = SO_INTERNAL_STATE_TERMINATED;
			SOiFree( SO_MEM_RESOLVER, soWork.pResolver,
					//OSRoundUp32B( sizeof( SOResolver ) ) );
					0x460);
			break;
		case SO_INTERNAL_STATE_ACTIVE:
			/* started */
			SOiReport( SO_REPORT_EINPROGRESS );
#define SO_EINPROGRESS -26
			result  = SO_EINPROGRESS;
			break;
	}

	PutLastError( (s32)result );
	(void)OSRestoreInterrupts(enabled);
	return result;
}

int SOStartup(void)
{
#define SO_STARTUP_TIMEOUT	0xEA60
	//SOiStartupCore(SO_STARTUP_TIMEOUT);
	SOStartupEx( SO_STARTUP_TIMEOUT );
}

//int SOiStartupCore(int value)
int SOStartupEx(int timeout)
{
	/* TODO - finish */
	return 0;
}

int SOCleanup(void)
{
	/* TODO - finish */
	return 0;
}

int SOGetLastError(void)
{
	OSThread* cur = OSGetCurrentThread();

	if (cur == NULL)
	{
		return (int)soError;
	}
	/* FIXME OSThread* struct */
	return (int)(cur->error);
}

BOOL SOiIsBufferAddrCheck(void)
{
	return soBufAddrCheck;
}

BOOL SOiIsInitialized(void)
{
	BOOL result = FALSE;
	BOOL enabled = OSDisableInterrupts();
	switch (soState)
	{
		case SO_INTERNAL_STATE_READY:
		case SO_INTERNAL_STATE_ACTIVE:
			result = TRUE;
			break;
	}
	(void)OSRestoreInterrupts(enabled);
	return result;
}

void* SOiAlloc(u32 name, s32 size)
{
	if ( (size > 0) && (soWork.alloc != NULL) )
	{
		void *ptr = soWork.alloc(name, size);
/* FIXME ASSERTMSG */
		ASSERTMSG( ( (u32)ptr & (u32)(32-1) ) == 0, "Allocator took Invalid align buffer");
		if (ptr != NULL)
		{
			soWork.allocCount++;
/* FIXME */
#define SO_IS_GDDR3_ADDR(x)  ( ((u32)x>= 0x90000000) && ((u32)x<0x94000000) )
			if (SOiIsBufferAddrCheck() && !SO_IS_GDDR3_ADDR(ptr) )
			{
				SOiReport("Allcator didn't take buffer from MEM2");
				SOiFree(name, ptr, size);
				ptr = NULL;
			}
		}
		return ptr;
	}
	return NULL;
}

void SOiFree(u32 name, void* ptr, s32 size)
{
	if ( (ptr != NULL) && (soWork.free != NULL) )
	{
		soWork.allocCount--;
		soWork.free(name, ptr, size);
	}
}

void SOiPrepare(void)
{
	/* TODO - finish */
}

int SOiConclude(const char* funcName, int result)
{
	(void) funcName;

	BOOL enabled = OSDisableInterrupts();
	PutLastError(result);
	(void)OSRestoreInterrupts(enabled);
	return result;
}

void SOiPrepareTempRm(void)
{
	/* TODO - finish */
}

void SOiConcludeTempRm(void)
{
	/* TODO - finish */
}

int SOiWaitForDHCPEx(int timeOut);
int SOiWaitForDHCP(void)
{
	return SOiWaitForDHCPEx(0);
}
int SOiWaitForDHCPEx(int timeOut)
{
	/* TODO - finish */
	return 0;
}

void PutLastError(s32 result)
{
	OSThread* cur;
	if ( (cur = OSGetCurrentThread() ) != NULL)
	{
		/* FIXME OSThread struct */
		cur->error = result;
	}
	else
	{
		soError = result;
	}
}

static int ConvNWC24Err(NWC24Err err, s32 exErr)
{
	int result = SO_EINVAL;
	switch (err)
	{
		case NWC24_OK:
			result = SO_SUCCESS;
			break;
		case -33: // NWC24_ERR_FAILED NWC24_ERR_CONFIG_NETWORK
		case -2:
			return exErr;
			break;
		case -29:
			result = -26;
			break;
		case -22:
		case -13:
			result = -48;
			break;
		case -1:
			result = 0x80000000;
			break;
	}
	return result;
}

void __SOCreateSocket(void)
{
	/* TODO - finish */
}

int SOClose(int s)
{
	int result;
	int rmId;

	if ( ( result = SO_PREPARE_FUNCTION( &rmId ) ) == SO_SUCCESS )
	{
		int * sock = (int*)SO_FUNCTION_ALLOC( sizeof( int ) );
		if (!sock)
		{
			result = SO_ENOMEM;
		}
		else
		{
			*sock = s;
			result = (int)IOS_Ioctl(rmId, NET_SO_CLOSE, (void*)sock, sizeof(int), NULL, 0);
			SO_FUNCTION_FREE(sock, sizeof(int));
		}
		result = SO_CONCLUDE_FUNCTION(result);
	}
	return result;
}

int SOConnect(int s, const void* sockAddr)
{
	int     result;
	s32     rmId;

	if( ( result = SO_PREPARE_FUNCTION( &rmId ) ) == SO_SUCCESS )
	{
		if( ( sockAddr == NULL ) ||
#ifdef  SO_INET6_SUPPORT
				( ( (SOSockAddr*)sockAddr )->len > sizeof( SOSockAddrIn6 ) ) ||
#else
				( ( (SOSockAddr*)sockAddr )->len > sizeof( SOSockAddrIn ) ) ||
#endif
				( ( (SOSockAddr*)sockAddr )->len < sizeof( SOSockAddrIn ) ) )
		{
			result  = SO_EINVAL;
		}
		else
		{
			NETSoMap*   netSoMap
				= (NETSoMap*)SO_FUNCTION_ALLOC( sizeof( NETSoMap ) );

			if( netSoMap == NULL )
			{
				result  = SO_ENOMEM;
			}
			else
			{
				netSoMap->sock      = s;
				netSoMap->validAddr = 1;
				(void)memcpy( netSoMap->addrBuf, sockAddr,
						( (SOSockAddr*)sockAddr )->len );
				result  = (int)IOS_Ioctl( rmId, NET_SO_CONNECT,
						(void*)netSoMap, sizeof( NETSoMap ), NULL, 0 );
				SO_FUNCTION_FREE( netSoMap, sizeof( NETSoMap ) );
			}
		}
		result  = SO_CONCLUDE_FUNCTION( result );
	}
	return result;
}

void SORecv(void)
{
	/* TODO - finish */
}

void SOSend(void)
{
	/* TODO - finish */
}

int SOShutdown(int s, int how)
{
	int     result;
	s32     rmId;

	if( ( result = SO_PREPARE_FUNCTION( &rmId ) ) == SO_SUCCESS )
	{
		NETSoShutdown*  netSoShutdown
			= (NETSoShutdown*)SO_FUNCTION_ALLOC( sizeof( NETSoShutdown ) );

		if( netSoShutdown == NULL )
		{
			result  = SO_ENOMEM;
		}
		else
		{
			netSoShutdown->sock     = s;
			netSoShutdown->how      = how;
			result  = (int)IOS_Ioctl( rmId, NET_SO_SHUTDOWN,
					(void*)netSoShutdown, sizeof( NETSoShutdown ), NULL, 0 );
			SO_FUNCTION_FREE( netSoShutdown, sizeof( NETSoShutdown ) );
		}
		result  = SO_CONCLUDE_FUNCTION( result );
	}
	return result;
}

u32 SONtoHl(u32 netshort)
{
	return netshort;
}
u16 SONtoHs(u16 netshort)
{
	return netshort;
}
u32 SOHtoNl(u32 hostlong)
{
	return hostlong;
}
u16 SOHtoNs(u16 hostshort)
{
	return hostshort;
}

void RecvFrom(void)
{
	/* TODO - finish */
}

void SendTo(void)
{
	/* TODO - finish */
}

long SOGetHostID(void)
{
	int     result;
	s32     rmId;
	long    id  =   SO_INADDR_ANY;

	if( ( result = SO_PREPARE_FUNCTION( &rmId ) ) == SO_SUCCESS )
	{
		id  = (long)IOS_Ioctl( rmId, NET_SO_GETHOSTID, NULL, 0, NULL, 0 );
		(void)SO_CONCLUDE_FUNCTION( result );
	}
	return id;
}

void SOGetAddrInfo(void)
{
	/* TODO - finish */
}

void SOFreeAddrInfo(void)
{
	/* TODO - finish */
}

void FixAddrInfo(void)
{
	/* TODO - finish */
}

void SOSetSockOpt(void)
{
	/* TODO - finish */
}

void SOGetInterfaceOpt(void)
{
	/* TODO - finish */
}

