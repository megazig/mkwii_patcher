#ifndef MEM_H

#define MEM_H

void MemFill(int * start, int * end, int value)
{
	while ( start < end)
	{
		*start = value;
		start++;
	}
}

#endif /* end of include guard: MEM_H */
