#ifndef COMMON_H

#define COMMON_H

int EndianSwap(int value)
{
	return ((value & 0xFF000000L) >> 24) |
		((value & 0x00FF0000L) >>  8) |
		((value & 0x0000FF00L) <<  8) |
		((value & 0x000000FFL) << 24);
}

int PowerOf2(int value)
{
	if (value < 0)
		return 0;
	if (value == 0)
		return 1;
	int foo = value & (value - 1);
	return ((value & (value-1)) == 0);
}

int abs(int value)
{
#ifndef USE_ASM
	return (x < 0) ? -x : x;
#else
	asm ( "srawi 4,3,0x1F; xor 0,4,3; subf 3,4,0;" : : );
#endif
}

/*
 * memmove
 * memchr
 * __memrchr
 * memcmp
 * __copy_longs_aligned
 * __copy_longs_rev_aligned
 * __copy_longs_unaligned
 * __copy_longs_rev_unaligned
 * __signbitd
 * parse_format
 * __pformatter
 * __va_args
 * long2str
 * longlong2str
 * __mod2u
 * __div2u
 * double2hex
 * strcpy
 * round_decimal
 * float2str
 * __num2dec
 * strchr
 * strlen
 * wcstombs
 */

#endif /* end of include guard: COMMON_H */
