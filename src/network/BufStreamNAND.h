#ifndef BUFSTREAMNAND_H

#define BUFSTREAMNAND_H

#include "BinStream.h"

class BufStreamNAND : public BinStream
{
};

bool BufStreamNAND::Fail(void)
{
	return this->fail;
}

bool BufStreamNAND::Eof(void)
{
	return (this->length - this->written_or_read == 0);
}

int BufStreamNAND::Tell(void)
{
	return this->stream_current;
}

void BufStreamNAND::Flush(void)
{
}

#endif /* end of include guard: BUFSTREAMNAND_H */
