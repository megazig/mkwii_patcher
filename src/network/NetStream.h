#ifndef NETSTREAM_H

#define NETSTREAM_H

float kConnectTimeout = 1000.0;

typedef struct NetAddress NetAddress;
struct NetAddress
{
	u32 hi;
	u16 lo;
};

class NetStream : public BinStream
{
	public:
		NetworkSocket * network_socket;
		bool fail;
		float time_to_live;
		int total_read;
		int total_written;

		NetStream(void);
		virtual ~NetStream(void);
		virtual void Flush(void);
		virtual int Tell(void);
		virtual int Eof(void);
		virtual bool Fail(void);
		//virtual const char * Name(void);	//BinStream::Name()
		//virtual Cached(void);				//BinStream::Cached()
		//virtual GetPlatform(void);		//BinStream::GetPlatform()
		virtual void ReadImpl(void * buffer, int length);
		virtual void WriteImpl(void const * buffer, int length);
		virtual void SeekImpl(int where, BinStream::SeekType type);

		int ClientConnect(NetAddress const & net_address);
};

NetStream::NetStream(void) : BinStream(1)
{
	this->fail = false;
	this->time_to_live = 0.0;
	this->total_read = 0;
	this->total_written = 0;
	this->network_socket = NetworkSocket::Create(1);
}

NetStream::~NetStream(void)
{
	if (this->network_socket)
	{
		this->network_socket->Disconnect();
		if (this->network_socket)
			delete this->network_socket;
	}
	/* TODO - BinStream dtor */
}

void NetStream::ClientConnect(NetAddress const & net_address)
{
	Timer_t timer;
	timer.Restart();
	this->network_socket->Connect(net_address.hi, net_address.lo);
	if (this->network_socket->Fail())
	{
		this->fail = true;
	}
	else
	{
		while (!this->network_socket->CanSend())
		{
			if (this->network_socket->Fail() || timer.SplitMs()>kConnectTimeout)
			{
				this->fail = true;
				break;
			}
		}
	}
}

int NetStream::Eof(void)
{
	return (this->network_socket->BytesAvailable() == 0);
}

/* TODO - maybe add real working stuffs */
void NetStream::SeekImpl(int r4, BinStream::SeekType type)
{
}

void NetStream::ReadImpl(void * buffer, int length)
{
	int toRead = length;
	u8* buf = (u8*)buffer;

	Timer_t timer;
	timer.Start();

	int read = 0;
	for ( ; toRead > 0 ; buf += read, toRead -= read )
	{
		read = this->network_socket->Read(buf, toRead);
		if (this->network_socket->Fail())
		{
			this->fail = true;
			break;
		}
		if (this->time_to_live!=0.0 && timer.SplitMs()>this->time_to_live)
		{
			this->fail = true;
			break;
		}
	}

	if (!this->fail)
	{
		this->total_read += length;
	}
}

void NetStream::WriteImpl(void const * buffer, int length)
{
	int toWrite = length;
	u8 * buf = (u8*)buffer;

	Timer_t timer;
	timer.Start();

	int written = 0;
	for ( ; toWrite > 0 ; buf += written, toWrite -= written )
	{
		written = this->network_socket->Send(buf, toWrite);
		if (this->network_socket->Fail())
		{
			this->fail = true;
			break;
		}
		if (this->time_to_live!=0.0 && timer.SplitMs()>this->time_to_live)
		{
			this->fail = true;
			break;
		}
	}

	if (!this->fail)
	{
		this->total_written += length;
	}
}

/* TODO - maybe add real working stuffs */
void NetStream::Flush(void)
{
}

bool NetStream::Fail(void)
{
	return this->fail;
}

int NetStream::Tell(void)
{
	return 0;
}

/*
 * NetworkSocket.h
 */
NetworkSocket * NetworkSocket::Create(bool foo)
{
	/* NOTE - ifdefd in real code */
	return new WiiNetworkSocket(foo);
}

#endif /* end of include guard: NETSTREAM_H */
