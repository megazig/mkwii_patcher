#ifndef MATH_H

#define MATH_H

class Vector3
{
	public:
		float x, y, z;

		Vector3(float x, float y, float z);
		Vector3(Vector3 const & vec);
};

Vector3::Vector3(float x, float y, float z)
{
	this.x = x;
	this.y = y;
	this.z = z;
}
Vecto3::Vector3(Vector3 const & vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}
void Vector3::Set(float x, float y, float z)
{
	this.x = x;
	this.y = y;
	this.z = z;
}

/*
void Multiply(Vector3 const & vec, Transform const & trans, Vector3 & vec)
{
// asm
}
*/

void Vector3::operand=(Vector3 const & vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

void Subtract(Vector3 const & vec1, Vector3 const & vec2, Vector3 & vec3)
{
	vec3.Set(
			vec1.x - vec2.x,
			vec1.y - vec2.y,
			vec1.z - vec2.z
			);
}

float Interp(float x, float y, float z)
{
	float foo = y - z;
	/* FIXME */
	return foo * (x+y);
}

void Scale(Vector3 const & vec1, float scale, Vector3 & vec2)
{
	vec2.Set(
			vec1.x * scale,
			vec1.y * scale,
			vec1.z * scale
			);
}

/* MORE */

int Clamp(int min, int max, int value)
{
	if (value > max)
		return max;
	return (value < min) ? min : value;
}

#endif /* end of include guard: MATH_H */
