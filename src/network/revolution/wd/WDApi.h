/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - libraries - wd
  File:     WDApi.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef WD_WDAPI_H_
#define WD_WDAPI_H_

#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

#include <revolution/wd/WDConst.h>
#include <revolution/wd/WDTypes.h>

typedef struct WDScanParam_
{
    u16         channelBitmap;
    u16         maxChannelTime;
    u8          bssid[ WD_SIZE_BSSID ];
    u16         scanType;
    
    u16         ssidLength;
    u8          ssid[ WD_SIZE_SSID ];
    u8          ssidMatchMask[ WD_SIZE_SSID ];

} __attribute__((packed)) WDScanParam;

u8  WDGetRadioLevel( const WDBssDesc* bssDesc );


/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif  /* __cplusplus */

#endif  /* WD_WDAPI_H_ */

/*---------------------------------------------------------------------------*
  $Log: WDApi.h,v $
  Revision 1.3  2006/09/27 09:34:41  terui
  Added WDGetRadioLevel() prototype.

  Revision 1.2  2006/08/30 00:05:39  adachi_hiroaki
  Added newlines to end of file to suppress gcc warnings

  Revision 1.1  2006/08/29 07:01:55  yoshioka_yasuhiro
  Initial Commit.

 *---------------------------------------------------------------------------*/
