/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - libraries - wd
  File:     WDConst.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef WD_WDCONST_H_
#define WD_WDCONST_H_

#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

/*---------------------------------------------------------------------------*
  Constant Definitions
 *---------------------------------------------------------------------------*/

#define     WD_SUCCESS              0
#define     WD_ERR_FATAL            (-1)
#define     WD_ERR_FAILURE          (-2)
#define     WD_ERR_ILLEGAL_PARAM    (-3)
#define     WD_ERR_ILLEGAL_STATUS   (-4)
#define     WD_ERR_INPROGRESS       (-5)
#define     WD_ERR_EXCLUSIVE        (-6)
#define     WD_ERR_INSUFFICIENT_BUF (-7)

#define     WD_SCAN_PRIVACY_MODE_NONE           0x0000    /* no encryption */
#define     WD_SCAN_PRIVACY_MODE_WEP40          0x0001    /* WEP (RC4 40 bit) encryption */
#define     WD_SCAN_PRIVACY_MODE_WEP104         0x0002    /* WEP (RC4 104 bit) encryption */
#define     WD_SCAN_PRIVACY_MODE_WPA_TKIP       0x0004    /* WPA-PSK (TKIP) encryption */
#define     WD_SCAN_PRIVACY_MODE_WPA2_AES       0x0005    /* WPA2-PSK (AES) encryption */
#define     WD_SCAN_PRIVACY_MODE_WPA_AES        0x0006    /* WPA-PSK (AES) encryption */
#define     WD_SCAN_PRIVACY_MODE_WPA2_TKIP      0x0007    /* WPA2-PSK (TKIP) encryption */
#define     WD_SCAN_PRIVACY_MODE_WEP            0x0008    /* WEP (key length unknown) encryption */

// Size of each type of data (bytes)
#define WD_SIZE_BSSID                       6
#define WD_SIZE_SSID                        32

typedef enum WDElementID
{
    WD_ELEM_SSID = 0,
    WD_ELEM_SUPPORTED_RATES,
    WD_ELEM_FH_PARAM_SET,
    WD_ELEM_DS_PARAM_SET,
    WD_ELEM_CF_PARAM_SET,
    WD_ELEM_TIM,
    WD_ELEM_IBSS_PARAM_SET,
    WD_ELEM_COUNTRY,
    WD_ELEM_HOPPING_PATTERN_PARAMS,
    WD_ELEM_HOPPING_PATTERN_TABLE,
    WD_ELEM_REQUEST,
    /* 11 - 15 reserved or unused */
    WD_ELEM_CHALLENGE_TEXT = 16,
    /* 17 - 31 reserved */
    WD_ELEM_POWER_CONSTRAINT = 32,
    WD_ELEM_POWER_CAPABILITY,
    WD_ELEM_TPC_REQUEST,
    WD_ELEM_TPC_REPORT,
    WD_ELEM_SUPPORTED_CHANNELS,
    WD_ELEM_CHANNEL_SWITCH_ANNOUNCEMENT,
    WD_ELEM_MEASUREMENT_REQUEST,
    WD_ELEM_MEASUREMENT_REPORT,
    WD_ELEM_QUIET,
    WD_ELEM_IBSS_DFS,
    WD_ELEM_ERP_INFO,
    /* 43 - 47 reserved */
    WD_ELEM_ROBUST_SECURITY_NETWORK = 48,
    /* 49 reserved */
    WD_ELEM_EXTENDED_SUPPORTED_RATES = 50
    /* 51 - 255 reserved or unused */
} WDElementID;

typedef s32 WDError;

/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif  /* __cplusplus */

#endif  /* WD_WDCONST_H_ */

/*---------------------------------------------------------------------------*
  $Log: WDConst.h,v $
  Revision 1.3  2006/08/31 02:07:44  yoshioka_yasuhiro
  Added WD_SCAN_PRIVACY_MODE_* and WDElementID.

  Revision 1.2  2006/08/30 00:05:39  adachi_hiroaki
  Added a carriage return at the end of the file to suppress gcc warnings.

  Revision 1.1  2006/08/29 07:01:55  yoshioka_yasuhiro
  Initial Commit.

 *---------------------------------------------------------------------------*/
