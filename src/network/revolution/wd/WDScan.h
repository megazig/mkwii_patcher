/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - libraries - wd
  File:     WDScan.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef WD_WDSCAN_H_
#define WD_WDSCAN_H_

#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

#include <revolution/wd/WDTypes.h>
#include <revolution/wd/WDApi.h>

WDError WDCheckEnableChannel( u16* scanEnableChannel );
WDError WDScanOnce( u16* buf, u16 bufLen, const WDScanParam* param );
u16     WDGetPrivacyMode( const WDBssDesc *bssDesc );

BOOL    WDFindGameInfo(u8** bufPtr, s32* length, const WDBssDesc* bssDesc);
BOOL    WDFindInformationElement(u8** bufPtr, s32* length, const WDBssDesc *bssDesc, WDElementID elementID);
BOOL    WDFindWifiProtectedAccess(u8** bufPtr, s32* length, const WDBssDesc* bssDesc);

BOOL    WDTryUsbApRegistrationOnce( u16 usbapChannels, BOOL usbapSignal, const u8* usbapNickname );

/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif  /* __cplusplus */

#endif  /* WD_WDSCAN_H_ */

/*---------------------------------------------------------------------------*
  $Log: WDScan.h,v $
  Revision 1.5  2006/09/27 09:34:02  terui
  Added WDFindGameInfo() prototype.

  Revision 1.4  2006/09/21 02:27:00  yoshioka_yasuhiro
  Added WDTryUsbApRegistrationOnce.

  Revision 1.3  2006/09/04 05:21:57  yoshioka_yasuhiro
  Deleted unnecessary declarations.

  Revision 1.2  2006/08/31 02:31:01  yoshioka_yasuhiro
  Deleted unnecessary declarations.

  Revision 1.1  2006/08/31 02:08:00  yoshioka_yasuhiro
  Initial Commit.

 *---------------------------------------------------------------------------*/
