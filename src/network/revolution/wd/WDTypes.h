/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - libraries - wd
  File:     WDTypes.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef WD_WDTYPES_H_
#define WD_WDTYPES_H_

#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

#include <revolution/wd/WDConst.h>

typedef struct WDBssDesc_
{
    u16         length;
    u16         rssi;
    u8          bssid[ WD_SIZE_BSSID ];
    u16         ssidLength;
    u8          ssid[ WD_SIZE_SSID];
    u16         capaInfo;
    struct
    {
        u16     basic;
        u16     support;
    } rateSet;
    u16         beaconPeriod;
    u16         dtimPeriod;
    u16         channel;
    u16         cfpPeriod;
    u16         cfpMaxDuration;
    u16         info_elt_len;
    u16         info_elt[1];
}  __attribute__((packed)) WDBssDesc;

/* Scan type */
typedef enum
{
    WD_SCANTYPE_ACTIVE  = 0,
    WD_SCANTYPE_PASSIVE = 1,
    WD_SCANTYPE_MAX,
    WD_SCANTYPE_INTMAX = 0xffffffff
} WDScanType;

/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif  /* __cplusplus */

#endif  /* WD_WDTYPES_H_ */

/*---------------------------------------------------------------------------*
  $Log: WDTypes.h,v $
  Revision 1.4  2006/09/04 05:22:18  yoshioka_yasuhiro
  Added WDScanType

  Revision 1.3  2006/08/30 11:44:34  yoshioka_yasuhiro
  Changed specifications of WDBssDesc

  Revision 1.2  2006/08/30 00:05:39  adachi_hiroaki
  Added newlines to end of file to suppress gcc warnings

  Revision 1.1  2006/08/29 07:01:55  yoshioka_yasuhiro
  Initial Commit.

 *---------------------------------------------------------------------------*/
