/*---------------------------------------------------------------------------*
  Project:  Revolution SDK Extensions
  File:     rexdefs.h

  Copyright 2006 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: rexdefs.h,v $
  Revision 1.1  2006/09/13 01:24:48  terui
  Initial upload

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_REXDEFS_H__
#define REVOLUTION_REXDEFS_H__

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/
#ifdef RVL_OS

#include <revolution/revodefs.h>

#ifdef	NDEBUG
#define REX_UNOFFICIAL_FUNC_VERSION( _libname_ )						\
	const char* __ ## _libname_ ## UnofficialVersion = "<< RVL_SDK - "	\
				#_libname_ " \trelease build: " __DATE__ " " __TIME__	\
				" (" REVOLUTION_LIB_CC ") UNOFFICIAL >>"
#else
#define REX_UNOFFICIAL_FUNC_VERSION( _libname_ )						\
	const char* __ ## _libname_ ## UnofficialVersion = "<< RVL_SDK - "	\
				#_libname_ " \tdebug build: " __DATE__ " " __TIME__		\
				" (" REVOLUTION_LIB_CC ") UNOFFICIAL >>"
#endif

#endif
/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_REXDEFS_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
#if 0
#ifndef __REVODEFS_H__
#define __REVODEFS_H__

#ifdef __cplusplus
extern "C" {
#endif

#define REVOLUTION_DEF_STR(X)      REVOLUTION_DEF_VAL(X)
#define REVOLUTION_DEF_VAL(X)      #X
#define REVOLUTION_DEF_CAT(X,Y)    X ## Y
#define REVOLUTION_LIB_CC          REVOLUTION_DEF_STR(__CWCC__) "_" REVOLUTION_DEF_STR(__CWBUILD__)

#ifdef _DEBUG
#define REVOLUTION_LIB_VERSION(lib)    \
    const char* __ ## lib ## Version = "<< RVL_SDK - " #lib " \tdebug build: " __DATE__ " " __TIME__ " (" REVOLUTION_LIB_CC ") >>"
#else
#define REVOLUTION_LIB_VERSION(lib)    \
    const char* __ ## lib ## Version = "<< RVL_SDK - " #lib " \trelease build: " __DATE__ " " __TIME__ " (" REVOLUTION_LIB_CC ") >>"
#endif

#ifdef __cplusplus
}
#endif

#endif  // __REVODEFS_H__
#endif
