/*---------------------------------------------------------------------------*
  Project:  NCD library
  File:     ncd.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law. They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: ncd.h,v $
  Revision 1.14  2008/01/22 09:28:15  seiki_masashi
  Added the NCDHasEnabledNetworkConfig function.

  Revision 1.13  2006/10/23 02:57:33  seiki_masashi
  Added NCDRestoreConfig function.

  Revision 1.12  2006/09/08 11:59:37  seiki_masashi
  Added NCDGetCurrentConfig().

  Revision 1.11  2006/08/24 08:21:15  yasu
  Split up functions for getting wireless MAC addresses in the console.

  Revision 1.10  2006/08/10 08:11:22  yasu
  Created nwc24cntl.h

  Revision 1.9  2006/08/09 03:45:09  terui
  Added the function NCDIsInterfaceDecided

  Revision 1.8  2006/08/09 00:34:02  yasu
  Added OSSleep*

  Revision 1.7  2006/07/29 04:19:19  terui
  Added the NCDSetNwc24Permission function definition

  Revision 1.6  2006/07/24 04:42:20  terui
  Moved the NCDLockWirelessDriver and NCDUnlockWirelessDriver prototype definitions from NCDPrivate.h

  Revision 1.5  2006/07/22 11:53:22  terui
  Deleted prototype declarations in step with the change of some of the APIs to private status
  Added prototype declarations in step with the addition of a number of APIs

  Revision 1.4  2006/06/26 09:19:06  terui
  Added NCDSleep function prototype.

  Revision 1.3  2006/06/16 07:37:57  terui
  Revised code in line with changes to the network config definition header

  Revision 1.2  2006/06/14 11:18:59  terui
  Added NCDGetLinkStatus function definition.
  Added NCDGetWirelessMacAddress function definition.

  Revision 1.1  2006/05/30 07:06:01  terui
  Initial upload.


  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_NCD_H__
#define REVOLUTION_NCD_H__

#include <revolution/types.h>
#include <revolution/os.h>
#include <revolution/ncd/NCDConst.h>
#include <revolution/ncd/NCDTypes.h>
#include <revolution/ncd/NCDMisc.h>

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/

/*---------------------------------------------------------------------------*
  Name        : NCDReadConfig
  Description : Load network configuration from NAND.
  Arguments   : config  - Specifies the buffer for storing the information to be read.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDReadConfig( NCDConfig* config );

/*---------------------------------------------------------------------------*
  Name        : NCDWriteConfig
  Description : Writes the network configuration to NAND.
  Arguments   : config  - Specifies the buffer for storing the information to be written.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDWriteConfig( const NCDConfig* config );

/*---------------------------------------------------------------------------*
  Name        : NCDRestoreConfig
  Description : Restores the current network setting to the value stored in NAND.
  Arguments   : None.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDRestoreConfig( void );

/*---------------------------------------------------------------------------*
  Name        : NCDGetLinkStatus
  Description : Gets the current state within automatic state transition of the NCD.
  Arguments   : None.
  Returns     : s32     - Returns the state ID if the state was successfully gotten.
                          Returns a negative number if get failed.
 *---------------------------------------------------------------------------*/
s32     NCDGetLinkStatus( void );

/*---------------------------------------------------------------------------*
  Name        : NCDIsInterfaceDecided
  Description : Checks whether preparations have been made for the network interface to be established and communicate with the NCD automatic state transition.
                
  Arguments   : None.
  Returns     : BOOL    -   Returns TRUE if network interface has been established.
                             Returns FALSE if the network interface has not been established or if the function failed.
                             
 *---------------------------------------------------------------------------*/
BOOL    NCDIsInterfaceDecided( void );

/*---------------------------------------------------------------------------*
  Name        : NCDLockWirelessDriver
  Description : Gets wireless driver usage rights and exclusively locks the driver.
  Arguments   : None.
  Returns     : s32     - Returns the usage rights ID if successfully gotten.
                          Returns a negative number if get failed.
 *---------------------------------------------------------------------------*/
s32     NCDLockWirelessDriver( void );

/*---------------------------------------------------------------------------*
  Name        : NCDUnlockWirelessDriver
  Description : Releases the exclusive lock on the wireless driver and destroys the usage rights.
  Arguments   : lockId  - Specifies the usage rights ID gotten when the driver was exclusively locked
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDUnlockWirelessDriver( s32 lockId );

/*---------------------------------------------------------------------------*
  Name        : NCDSleep
  Description : Puts the thread into sleep mode for the specified length of time.
  Arguments   : tick    - Specifies the amount of time to sleep as an OSTime type variable.
  Returns     : None.
 *---------------------------------------------------------------------------*/
void    NCDSleep( OSTime tick );
#ifndef OSSleepSeconds
void    OSSleepTicks( OSTime ticks );
#define OSSleepSeconds( sec )           OSSleepTicks( OSSecondsToTicks(sec) )
#define OSSleepMilliseconds( msec )     OSSleepTicks( OSMillisecondsToTicks(msec) )
#define OSSleepMicroseconds( usec )     OSSleepTicks( OSMicrosecondsToTicks(usec) )
#define OSSleepNanoseconds( nsec )      OSSleepTicks( OSNanosecondsToTicks(nsec) )
#endif

/*---------------------------------------------------------------------------*
  Name        : NCDSetIfConfig
  Description : Changes the settings relating to the network interface.
                This does not get reflected in NAND.
  Arguments   : ifconfig     - The pointer to the network interface settings structure.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDSetIfConfig( const NCDIfConfig* ifconfig );

/*---------------------------------------------------------------------------*
  Name        : NCDSetIpConfig
  Description : Changes TCP/IP-related settings.
                This does not get reflected in NAND.
  Arguments   : ifconfig     - The pointer to the TCP/IP-related settings structure.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDSetIpConfig( const NCDIpConfig* ipconfig );

/*---------------------------------------------------------------------------*
  Name        : NCDGetCurrentConfig
  Description : Gets the current network configuration.
  Arguments   : ifconfig     - The pointer to the network interface settings structure.
                ifconfig     - The pointer to the TCP/IP-related settings structure.
                profileIndex -  Pointer to the address storing the profile number being used
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDGetCurrentConfig( NCDIfConfig* ifconfig, NCDIpConfig* ipconfig,
                             s32* profileIndex );

/*---------------------------------------------------------------------------*
  Name        : NCDGetCurrentIfConfig
  Description : Gets the network interface-related settings that are currently in use.
  Arguments   : ifconfig     - The pointer to the network interface settings structure.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDGetCurrentIfConfig( NCDIfConfig* ifconfig );

/*---------------------------------------------------------------------------*
  Name        : NCDGetCurrentIpConfig
  Description : Gets the TCP/IP-related settings that are currently in use.
  Arguments   : ifconfig     - The pointer to the TCP/IP-related settings structure.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDGetCurrentIpConfig( NCDIpConfig* ipconfig );

/*---------------------------------------------------------------------------*
  Name        : NCDHasEnabledNetworkConfig
  Description : Checks if currently enabled network settings exist.
  Arguments   : None.
  Returns     : BOOL        -   TRUE if it exists
 *---------------------------------------------------------------------------*/
BOOL NCDHasEnabledNetworkConfig( void );

/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_NCD_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
