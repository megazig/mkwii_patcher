/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - Network base library
  File:     net.h

  Copyright (C)2006 Nintendo  All Rights Reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
  
  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
    Header file for network base API.
 *---------------------------------------------------------------------------*/
 
#ifndef __NET_H__
#define __NET_H__

#include <revolution/net/NETErrorCode.h>
#include <revolution/net/NETDigest.h>
#include <revolution/net/NETCrypto.h>
#include <revolution/net/NETMisc.h>

#ifdef RVL_OS
#include <revolution/os.h>
#endif // RVL_OS

#ifdef __cplusplus
extern "C" {
#endif
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif // __NET_H__

