/*---------------------------------------------------------------------------*
  Project:  MP Data Sharing library
  File:     mpds.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: mpds.h,v $
  Revision 1.6  2007/11/29 03:18:20  seiki_masashi
  Added the const modifier.

  Revision 1.5  2007/10/24 13:46:41  seiki_masashi
  Changed so that OSThreadQueue is used during synchronization.

  Revision 1.4  2007/10/22 09:41:57  seiki_masashi
  Made the MPDSStep function into a synchronous function, and added the new function MPDSTryStep

  Revision 1.3  2007/10/17 12:47:21  seiki_masashi
  Changed the constants for debug output

  Revision 1.2  2007/10/17 09:37:34  seiki_masashi
  Added constants for debug output

  Revision 1.1  2007/10/10 08:38:07  seiki_masashi
  Added the MPDS library

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_MPDS_H__
#define REVOLUTION_MPDS_H__

#include <revolution/types.h>
#include <revolution/mp.h>

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/
/* Constants */

#define MPDS_HEADER_SIZE        4

#define MPDS_DATA_SIZE          508     // Maximum data size of one WMDataSet (512-4)
                                        // MPDS_DATA_SIZE+MPDS_HEADER_SIZE must be a multiple of 32

#define MPDS_DATASET_NUM        4       // How many MPDSDataSet elements to be kept in MPDSDataSetBuf (fixed value)
#define MPDS_CONTEXT_SIZE       (sizeof(MPDSContext))     // Buffer size for DataSharing

typedef enum
{
    MPDS_DEBUG_LEVEL_NONE        = 0,
    MPDS_DEBUG_LEVEL_TRACE       = 1,
    MPDS_DEBUG_LEVEL_REPORT      = 2,
    MPDS_DEBUG_LEVEL_WARNING     = 4,
    MPDS_DEBUG_LEVEL_ERROR       = 8,
    MPDS_DEBUG_LEVEL_DETAIL_AIDBITS = 16
} MPDSDebugLevel;

#define MPDS_DEBUG_LEVEL_DEFAULT (MPDS_DEBUG_LEVEL_WARNING|MPDS_DEBUG_LEVEL_ERROR)

/*===========================================================================*/
/* Declarations */

typedef struct MPDSConfig
{
    u32         dataLength;

    u32         port;
    u32         priority;

    BOOL        isParent;
    u32         aidBits;
    BOOL        isDoubleMode;

    BOOL        isAutoStart;
    MPPortCallback mpdsCallback;
}
MPDSConfig;

typedef struct MPDSDataSet
{
    u16     aidBits;
    u16     receivedBits;              // Receive status
    u16     data[MPDS_DATA_SIZE / sizeof(u16)]; // Shared data
}
MPDSDataSet ATTRIBUTE_ALIGN(2);

typedef struct MPDSContext
{
    MPDSConfig  config;

    MPDSDataSet ds[MPDS_DATASET_NUM];
    u16     seqNum[MPDS_DATASET_NUM]; // Sequential Number

    u16     writeIndex;                // Next Index to write
    u16     sendIndex;                 // Index currently being sent
    u16     readIndex;                 // Next Index to read

    u16     stationNumber;             // Number of devices sharing data (number of bits set to 1 in aidBits)
    u16     dataSetLength;             // dataLength * stationNumber
    u16     currentSeqNum;             // SeqNum of the DataSet read most recently
    u16     state;                     // Current DataSharing state (MPDSState)

    u8      padding[2];

    OSThreadQueue readThreadQueue;     // ThreadQueue for controlling synchronous reads
    MPPortCallbackInfo tmpCallbackInfo;// Work buffer for callbacks

}
MPDSContext;


/*===========================================================================*/
/* Functions */

/*---------------------------------------------------------------------------*
  Name        : MPDSInit
  Description : Configure the initial settings for data sharing
  Arguments   : context - The data sharing context structure
                config  - Data sharing settings
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32
MPDSInit( MPDSContext* context, const MPDSConfig* config );

/*---------------------------------------------------------------------------*
  Name        : MPDSSetupPortConfig
  Description : Configure MP communications for data sharing
  Arguments   : context - The data sharing context structure
                mpconfig - MP communication settings
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32
MPDSSetupPortConfig( const MPDSContext* context, MPConfig* mpconfig );

/*---------------------------------------------------------------------------*
  Name        : MPDSStep
  Description : Perform data sharing
  Arguments   : context - The data sharing context structure
                sendData - The data that the local host wants to share next
                recvDataSet - The data that was shared
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32
MPDSStep( MPDSContext* context, const void* sendData, MPDSDataSet* recvDataSet );

/*---------------------------------------------------------------------------*
  Name        : MPDSTryStep
  Description : Perform data sharing
  Arguments   : context - The data sharing context structure
                sendData - The data that the local host wants to share next
                recvDataSet - The data that was shared
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32
MPDSTryStep( MPDSContext* context, const void* sendData, MPDSDataSet* recvDataSet );

/*---------------------------------------------------------------------------*
  Name        : MPDSGetData
  Description : Gets the data that was shared
  Arguments   : context - The data sharing context structure
                dataSet - a shared data structure
                aid     - the aid of the data to be obtained
  Returns     : u8*     - a pointer to the shared data
 *---------------------------------------------------------------------------*/
const u8* MPDSGetData( const MPDSContext* context, const MPDSDataSet* dataSet, u32 aid );

#ifndef NDEBUG
/*---------------------------------------------------------------------------*
  Name        : MPDSSetDebugLevel
  Description : Change the level of debug output.
  Arguments   : level - Debug output type bit to be output
  Returns     : None.
 *---------------------------------------------------------------------------*/
void MPDSSetDebugLevel( u32 level );
#else
#define MPDSSetDebugLevel( level )    ( (void)0 )
#endif


/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_MP_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
