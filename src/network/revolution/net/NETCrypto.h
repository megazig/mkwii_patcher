/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - Network base library
  File:     NETCrypto.h

  Copyright (C)2006 Nintendo  All Rights Reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
  
  $Log: NETCrypto.h,v $
  Revision 1.6  2006/12/11 07:15:31  yosizaki
  fixed around definition of NETAESBlockMode.

  Revision 1.5  2006/12/11 04:17:48  yosizaki
  added NETAESBlockMode.

  Revision 1.4  2006/10/27 07:59:55  yosizaki
  changed AES context.

  Revision 1.3  2006/10/14 00:06:42  yosizaki
  added paddding for u64 in SHA1Context.

  Revision 1.2  2006/10/12 03:39:10  yosizaki
  changed prototypes of ECC.

  Revision 1.1  2006/10/11 10:06:02  yosizaki
  initial upload.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
    Header file for network base API.
 *---------------------------------------------------------------------------*/
 
#ifndef __NETCRYPTO_H__
#define __NETCRYPTO_H__

#ifdef RVL_OS
#include <revolution/os.h>
#else
#include <rvl/iosc.h>
#endif // RVL_OS

#include <revolution/net/NETDigest.h>

#ifdef __cplusplus
extern "C" {
#endif
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* crypto functions */

BOOL NETLockCrypto(void);
void NETUnlockCrypto(void);
BOOL NETIsCryptoAvailable(void);


/*---------------------------------------------------------------------------*/
/* AES encrypt / decrypt functions */

struct NETAESContext;
struct NETAESBlockModeInterface;

#define NET_AES_128_ROUND_MAX   (u32)((128 / 32) + 6)
#define NET_AES_192_ROUND_MAX   (u32)((192 / 32) + 6)
#define NET_AES_256_ROUND_MAX   (u32)((256 / 32) + 6)

#define NET_AES_BLOCK_LENGTH    16UL
#define NET_AES_BLOCK_WORDS     (u32)(NET_AES_BLOCK_LENGTH / sizeof(u32))


typedef struct NETAESBlockModeInterface
{
    void (*Encrypt)(struct NETAESContext *context, void *dst, const void *src);
    void (*Decrypt)(struct NETAESContext *context, void *dst, const void *src);
}
NETAESBlockModeInterface;
typedef const NETAESBlockModeInterface *NETAESBlockMode;

extern const NETAESBlockModeInterface NET_AES_BLOCK_MODE_CBC[1];
extern const NETAESBlockModeInterface NET_AES_BLOCK_MODE_OFB[1];


typedef struct NETAESContext
{
/* private: */
    u32     iv[NET_AES_BLOCK_WORDS];
    u32     roundkey[NET_AES_BLOCK_WORDS * (NET_AES_256_ROUND_MAX + 1)];
    u32     round:8;
    u32     encrypt:1;
    u32     flags:23;
    const NETAESBlockModeInterface *mode;
}
NETAESContext;


BOOL NETAESCreate(NETAESContext *context, const void *key, u32 keylen, const void *iv);
BOOL NETAESCreateEx(NETAESContext *context, const void *key, u32 keylen,
                    const void *iv, NETAESBlockMode mode);
void NETAESDelete(NETAESContext *context);
BOOL NETAESEncrypt(NETAESContext *context, void *dst, const void *src, u32 len);
BOOL NETAESDecrypt(NETAESContext *context, void *dst, const void *src, u32 len);


/* for internal use */
void AESiEncryptBlock(NETAESContext *context, u32 *dst, const u32 *src);
void AESiDecryptBlock(NETAESContext *context, u32 *dst, const u32 *src);


/*---------------------------------------------------------------------------*/
/* ECC sign / verify functions */

typedef struct NETECCSignInfo
{
/* private: */
    u8 cert[384];
    u8 signature[60];
}
NETECCSignInfo;

BOOL NETCreateSign(const void *buffer, u32 length, NETECCSignInfo *sign);
BOOL NETVerifySign(const void *buffer, u32 length, const NETECCSignInfo *sign);

typedef struct NETECCContext
{
/* private: */
    NETSHA1Context      sha1[1];
}
NETECCContext;

BOOL NETECCCreate(NETECCContext *context);
void NETECCDelete(NETECCContext *context);
BOOL NETECCUpdate(NETECCContext *context, const void *src, u32 len);
BOOL NETECCGetSign(NETECCContext *context, NETECCSignInfo *sign);
BOOL NETECCVerifySign(NETECCContext *context, const NETECCSignInfo *sign);


/*---------------------------------------------------------------------------*/
/* RSA verify functions */

#ifndef RVL_OS

typedef struct NETRSAContext
{
/* private: */
    IOSCPublicKeyHandle handle;
    u8                  padding[4];
    NETSHA1Context      sha1[1];
}
NETRSAContext;

IOSError NETRSACreate(NETRSAContext *context, const void *publickey, int exponent);
void     NETRSADelete(NETRSAContext *context);
void     NETRSAUpdate(NETRSAContext *context, const void *message, u32 length);
IOSError NETRSAVerify(NETRSAContext *context, const void *sign);

#endif /* RVL_OS */


/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // __NETCRYPTO_H__
