/*---------------------------------------------------------------------------*
  Project:  Wii Connect 24
  File:     NETErrorCode.h

  Copyright (C) 2006 Nintendo  All Rights Reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: NETErrorCode.h,v $
  Revision 1.4  2006/09/12 14:47:31  seiki_masashi
  Support for changes in organization of error codes

  Revision 1.3  2006/09/11 12:32:24  seiki_masashi
  Changed NET_ECODE_ADDR_COLLISION to -152700

  Revision 1.2  2006/09/11 08:53:30  seiki_masashi
  Support for NET_ECODE_AP_NOT_FOUND

  Revision 1.1  2006/09/08 12:00:22  seiki_masashi
  Split net.h multiple header files

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
    Error codes used for Network APIs
 *---------------------------------------------------------------------------*/
#ifndef __NETERRORCODE_H__
#define __NETERRORCODE_H__

#ifdef RVL_OS
#include <revolution/os.h>
#endif // RVL_OS

#ifdef __cplusplus
extern "C" {
#endif
/*---------------------------------------------------------------------------*/

typedef enum NETErrorCode
{
    NET_ECODE_OK                        = 0,

    NET_ECODE_STARTUP_FAILED            = -50100, // Failed to initialize network for unknown reason
    NET_ECODE_NO_ENABLED_CONFIG         = -50200, // Valid network configuration does not exist
    NET_ECODE_INVALID_CONFIG            = -50300, // Network configuration invalid
    NET_ECODE_DEVICE_NOT_EXISTS         = -50400, // No device (LAN adaptor not connected)

    NET_ECODE_AP_NOT_FOUND              = -51000, // Cannot find set access point
    NET_ECODE_WIRELESS_LINK_UP_FAILED   = -51300, // Could not connect with wireless network device for some reason
    NET_ECODE_WIRED_LINK_UP_FAILED      = -51400, // Could not connect with wired network device for some reason

    NET_ECODE_DHCP_FAILED               = -52000, // DHCP failed
    NET_ECODE_DNS_FAILED                = -52100, // DNS failed
    NET_ECODE_WRONG_PROXY               = -52400, // Could not connect to proxy server
    NET_ECODE_PROXY_AUTH_FAILED         = -52500, // Proxy server authentication failed
    NET_ECODE_ADDR_COLLISION            = -52700, // IP address collision

    NET_ECODE_NETWORK_DOWN              = -54000  // Disconnected from network during transmission

} NETErrorCode;

typedef enum NETConnectionType
{
    NET_CTYPE_WIRED                 = 20,
    NET_CTYPE_WIRELESS              = 30,
    NET_CTYPE_USBAP                 = 40,
    NET_CTYPE_UNKNOWN               = 99
} NETConnectionType;

#define NET_CONSTRUCT_ERROR_CODE(etype, ctype) ((etype)-(ctype))

#define NET_CTYPE_IS_WIRED(ctype) (NET_CTYPE_WIRED<=(ctype) && (ctype)<NET_CTYPE_WIRELESS)

/*---------------------------------------------------------------------------*/

s32 NETGetConnectionType( void );
s32 NETGetStartupErrorCode( s32 soErr );
s32 NETGetStartupErrorCodeEx( s32 soErr, s32 connType );

/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif  // __NWC24ERR_H__

