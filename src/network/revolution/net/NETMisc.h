/*---------------------------------------------------------------------------*
  Project:  RevolutionSDK Extensions - Network base library
  File:     NETMisc.h

  Copyright (C) 2006, 2007 Nintendo. All Rights Reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: NETMisc.h,v $
  Revision 1.13  2007/10/26 09:28:51  seiki_masashi
  Added NETSwapAndCopyMemory16().

  Revision 1.12  2007/09/07 00:50:37  hirose_kazuki
  Added NETMemSet().

  Revision 1.11  2007/09/06 02:36:12  hirose_kazuki
  Added NETMemCpy.

  Revision 1.10  2007/02/14 07:43:05  hirose_kazuki
  Removed some functions.

  Revision 1.9  2007/01/31 02:49:06  okubata_ryoma
  Small fix

  Revision 1.8  2007/01/31 02:32:05  okubata_ryoma
  Changed NCDGetWirelessMacAddress to NETGetWirelessMacAddress

  Revision 1.7  2006/10/26 11:00:43  yasu
  Added a 'get version information' function for REX.

  Revision 1.6  2006/09/19 12:40:44  terui
  Added NETIsValidVersion() prototype declaration

  Revision 1.5  2006/09/16 06:32:01  hirose_kazuki
  Fixed a typo in definition of NETGetUniversalSeconds().

  Revision 1.4  2006/09/15 12:06:15  shirakae
  Renamed NETGetUniversalSecond() to NETGetUniversalSeconds()

  Revision 1.3  2006/09/15 12:00:42  shirakae
  Renamed NETGetUniversalTime() to NETGetUniversalSecond()

  Revision 1.2  2006/09/15 10:27:26  adachi_hiroaki
  Implemented universal time APIs.

  Revision 1.1  2006/09/08 12:00:22  seiki_masashi
  Split net.h into multiple header files

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*
    Header file for network misc API.
 *---------------------------------------------------------------------------*/
 
#ifndef __NETMISC_H__
#define __NETMISC_H__

#ifdef RVL_OS
#include <revolution/os.h>
#endif // RVL_OS

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NET_MACADDRESS_LENGTH   6

/*---------------------------------------------------------------------------*/

#ifdef RVL_OS
s32     NETGetWirelessMacAddress( u8* pAddr );

#if 0   // no more supported
BOOL NETIsValidVersion( void );
#endif
u8*     NETGetRexIOPVersion( void );
u8*     NETGetRexPPCVersion( void );
char*   NETGetRexPPCVersionPrintable( void );

void*   NETMemCpy( void* dst, const void* src, size_t size );
void*   NETMemSet( void* buf, int ch, size_t size );
#endif // RVL_OS

void NETSwapAndCopyMemory16(void* dst, const void* src, u32 len);

/*---------------------------------------------------------------------------*/

static inline u32 NETMinU32(u32 a, u32 b)
{
    return (a <= b) ? a : b;
}

/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // __NETMISC_H__

