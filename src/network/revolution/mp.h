/*---------------------------------------------------------------------------*
  Project:  MP library
  File:     mp.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law. They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: mp.h,v $
  Revision 1.39  2008/06/05 09:51:23  seiki_masashi
  Added the MPWaitPseudoVBlank function.

  Revision 1.38  2008/06/03 03:25:37  seiki_masashi
  Fixed a bug that prevented 'indication' notifications from being sent normally.
  Added the pseudo V-Blank notification.

  Revision 1.37  2007/10/24 13:46:26  seiki_masashi
  Changed the method of passing ssid information when a child device connection is made.

  Revision 1.36  2007/10/24 01:54:51  seiki_masashi
  Added the MP_SIZE_SSID constant.

  Revision 1.35  2007/10/23 13:01:47  seiki_masashi
  Added the MPCountPopulation function.

  Revision 1.34  2007/10/22 09:42:22  seiki_masashi
  Added constants for the MPDS library.

  Revision 1.33  2007/10/10 08:36:37  seiki_masashi
  Added the MPSetPortConfigEx function.

  Revision 1.32  2007/10/04 09:38:48  seiki_masashi
  Support for MP_BEACON_PERIOD_AUTO.

  Revision 1.31  2007/10/03 05:04:24  seiki_masashi
  Changed asynchronous function arguments to directly take void* userData.

  Revision 1.30  2007/09/25 14:26:04  seiki_masashi
  Added MP_FATAL_ERROR_INTERNAL_ERROR.

  Revision 1.29  2007/08/24 04:43:01  seiki_masashi
  Added threads for each callback type.
  Removed MPCommandBlock.

  Revision 1.28  2007/01/16 08:46:47  seiki_masashi
  Corrected processing of restBits.

  Revision 1.27  2007/01/16 07:03:12  seiki_masashi
  Corrected processing of restBits.

  Revision 1.26  2007/01/15 10:25:08  seiki_masashi
  Implemented sequential communications.

  Revision 1.25  2006/11/24 07:37:41  seiki_masashi
  Made changes to add padding to MPConfig so that the size will be a multiple of 32 bytes.

  Revision 1.24  2006/11/13 09:37:13  seiki_masashi
  Added MPSwitchBufferAddrCheck function.

  Revision 1.23  2006/09/26 12:11:07  seiki_masashi
  Changed code to use semaphores during requests.

  Revision 1.22  2006/09/15 12:59:43  seiki_masashi
  Small fix

  Revision 1.21  2006/09/07 13:59:42  seiki_masashi
  Changed debug output.
  Changed result codes.

  Revision 1.20  2006/09/02 12:33:55  seiki_masashi
  Added the MPDisconnectAll function

  Revision 1.19  2006/08/28 12:34:39  seiki_masashi
  Changed the SwapBytes function to a function in the NET library.

  Revision 1.18  2006/08/14 14:37:53  yasu
  Support for PADDING warnings.

  Revision 1.17  2006/08/03 09:12:13  seiki_masashi
  typedef added to enum

  Revision 1.16  2006/07/04 00:22:52  seiki_masashi
  Added the const modifier to the const pointer.

  Revision 1.15  2006/07/03 13:35:27  seiki_masashi
  Added Life Time.

  Revision 1.14  2006/07/03 09:28:44  seiki_masashi
  Cleaned up error codes.

  Revision 1.13  2006/07/03 08:37:04  seiki_masashi
  Changed specifications of MPGetLinkLevel.

  Revision 1.12  2006/07/03 00:20:23  seiki_masashi
  Changed specifications so that automatic channel selection of MP_CHANNEL_AUTO is left up to WD.

  Revision 1.11  2006/07/02 13:03:19  seiki_masashi
  Added link level calculation.

  Revision 1.10  2006/07/02 11:51:28  seiki_masashi
  Added MP_CHANNEL_AUTO.
  Changed MP_DISCONNECT_REASON_*.

  Revision 1.9  2006/06/29 05:41:43  adachi_hiroaki
  Added MPDisconnect.

  Revision 1.8  2006/06/28 12:48:56  seiki_masashi
  Small fix

  Revision 1.7  2006/06/26 13:08:32  seiki_masashi
  Small fix

  Revision 1.6  2006/06/23 04:57:01  seiki_masashi
  Changed specifications so that FatalError notifications are made to the indicationCallbackFunction.

  Revision 1.5  2006/06/23 04:40:05  seiki_masashi
  Changed so that priority can be set.

  Revision 1.4  2006/06/23 02:27:36  seiki_masashi
  Deleted prio from the arguments of the MPSend function.

  Revision 1.3  2006/06/22 00:49:02  seiki_masashi
  Revised code related to sends.

  Revision 1.2  2006/06/21 02:58:30  seiki_masashi
  Various revisions

  Revision 1.1  2006/06/16 13:03:02  seiki_masashi
  Initial commit

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_MP_H__
#define REVOLUTION_MP_H__

#include <revolution/types.h>
#include <revolution/net.h>

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/

#define MP_RAW_IF 1 // enable raw MP send/receive functions for testing
#define MP_USE_SEMAPHORE_FOR_REQUEST

/*===========================================================================*/

// The range of MPResult is 0 and the negative numbers.
#define MP_RESULT_MP_ERROR_FLAG             0xffffff00
#define MP_RESULT_WD_ERROR_FLAG             0x80008000
typedef enum
{
    MP_RESULT_OK                          = 0,
    MP_RESULT_CLOSED                      = ( MP_RESULT_MP_ERROR_FLAG | 0x00 ), // -256
    MP_RESULT_ILLEGAL_PARAMETER           = ( MP_RESULT_MP_ERROR_FLAG | 0x01 ), // -255
    MP_RESULT_ALREADY_IN_USE              = ( MP_RESULT_MP_ERROR_FLAG | 0x06 ), // -250
    MP_RESULT_NOT_ENOUGH_MEMORY           = ( MP_RESULT_MP_ERROR_FLAG | 0x10 ), // -240
    MP_RESULT_NOT_ENOUGH_RESOURCE         = ( MP_RESULT_MP_ERROR_FLAG | 0x11 ), // -239
    MP_RESULT_NOT_ALLOWED                 = ( MP_RESULT_MP_ERROR_FLAG | 0x12 ), // -238
    MP_RESULT_BUSY                        = ( MP_RESULT_MP_ERROR_FLAG | 0x13 ), // -237
    MP_RESULT_ILLEGAL_STATE               = ( MP_RESULT_MP_ERROR_FLAG | 0x14 ), // -236
    MP_RESULT_TIMEOUT                     = ( MP_RESULT_MP_ERROR_FLAG | 0x15 ), // -235
    MP_RESULT_NO_DATA                     = ( MP_RESULT_MP_ERROR_FLAG | 0x16 ), // -234
    MP_RESULT_FAILURE                     = ( MP_RESULT_MP_ERROR_FLAG | 0x17 ), // -233
    MP_RESULT_FATAL_ERROR                 = ( MP_RESULT_MP_ERROR_FLAG | 0xff ), // -1
    MP_RESULT_WD_CLOSED                   = ( MP_RESULT_WD_ERROR_FLAG | 0x00 ),
    MP_RESULT_WD_ILLEGAL_PARAMETER        = ( MP_RESULT_WD_ERROR_FLAG | 0x01 ),
    MP_RESULT_WD_UNAVAILABLE_COMMAND      = ( MP_RESULT_WD_ERROR_FLAG | 0x02 ),
    MP_RESULT_WL_FAILURE                  = ( MP_RESULT_WD_ERROR_FLAG | 0x03 ),
    MP_RESULT_WD_INSUFFICIENT_BUFFER      = ( MP_RESULT_WD_ERROR_FLAG | 0x04 ),
    MP_RESULT_WD_FAIL_IOBUF               = ( MP_RESULT_WD_ERROR_FLAG | 0x05 ),
    MP_RESULT_WD_ALREADY_IN_USE           = ( MP_RESULT_WD_ERROR_FLAG | 0x06 )
} MPResult;

typedef enum
{
    MP_FATAL_ERROR_NONE                   = 0,
    MP_FATAL_ERROR_NOT_ENOUGH_MEMORY      = 1,
    MP_FATAL_ERROR_INTERNAL_ERROR         = 2,
    MP_FATAL_ERROR_UNKNOWN                = 0xff
} MPFatalError;

#define MP_CHILD_MAX                15
#define MP_CHILD_BITS_MASK          0x0000fffe
#define MP_AID_MAX                  (MP_CHILD_MAX+1)
#define MP_AID_BITS_MASK            0x0000ffff
#define MP_PORT_MAX                 16
#define MP_PORT_BROADCAST           0xffffffff
#define MP_RAW_PORT_MAX             8
#define MP_SEQ_PORT_MAX             8
#define MP_DATA_MAX                 512

#define MP_LINKLEVEL_MAX            4

#define MP_CHANNEL_AUTO             0
#define MP_LIFETIME_DEFAULT         4000
#define MP_TGID_AUTO                0xffffffff
#define MP_BEACON_PERIOD_AUTO       0

#define MP_SIZE_MACADDRESS          6
#define MP_SIZE_SSID                32
#define MP_SIZE_SSID_INTERNAL       8
#define MP_SIZE_SSID_USER_DATA      24
#define MP_GAMEINFO_USER_SIZE_MAX   112

#define MP_GAMEINFO_MAGIC_NUMBER    0x0001
#define MP_GAMEINFO_VERSION_NUMBER  1
#define MP_GAMEINFO_PLATFORM_ID_NITRO       0
#define MP_GAMEINFO_PLATFORM_ID_REVOLUTION  8

#define MP_ATTR_ENTRY_SHIFT     0
#define MP_ATTR_MB_SHIFT        1
#define MP_ATTR_FLAG_ENTRY      (1 << MP_ATTR_ENTRY_SHIFT)
#define MP_ATTR_FLAG_MB         (1 << MP_ATTR_MB_SHIFT)

typedef enum
{
    MP_MODE_NONE               = 0x00,
    MP_MODE_PARENT             = 0x01, // MP parent device mode

    MP_MODE_ENABLE_PSEUDO_VBLANK_INDICATION = 0x10000
} MPMode;

#define MP_PRIORITY_LEVEL       4      // 4 priority levels
typedef enum MPPriorityLevel
{
    MP_PRIORITY_URGENT = 0,
    MP_PRIORITY_HIGH,
    MP_PRIORITY_NORMAL,
    MP_PRIORITY_LOW
} MPPriorityLevel;

// MPCallbackType is a non-zero, positive number. This separates it from the range of MPResult.
#define MP_CB_GROUP_MASK            0xffff0000
#define MP_CB_GROUP_PORT            0x00000000
#define MP_CB_GROUP_ASYNC           0x00010000
#define MP_CB_GROUP_INDICATION      0x00020000
#define MP_CB_GROUP_MPDS            0x00030000
typedef enum
{
    MP_PORT_CB_TYPE_NONE            = (MP_CB_GROUP_PORT  | 0x00), // unused
    MP_PORT_CB_TYPE_STARTUP         = (MP_CB_GROUP_PORT  | 0x01),
    MP_PORT_CB_TYPE_CLEANUP         = (MP_CB_GROUP_PORT  | 0x02),
    MP_PORT_CB_TYPE_DATA_RECEIVED   = (MP_CB_GROUP_PORT  | 0x03),
    MP_PORT_CB_TYPE_CONNECTED       = (MP_CB_GROUP_PORT  | 0x04),
    MP_PORT_CB_TYPE_DISCONNECTED    = (MP_CB_GROUP_PORT  | 0x05),
    MP_ASYNC_CB_TYPE_DATA_SENT      = (MP_CB_GROUP_ASYNC | 0x01),
    MP_ASYNC_CB_TYPE_BEACON_SENT    = (MP_CB_GROUP_ASYNC | 0x02),
    MP_INDICATION_CB_TYPE_PSEUDO_VBLANK = (MP_CB_GROUP_INDICATION | 0x01),
    MP_INDICATION_CB_TYPE_FATAL_ERROR = (MP_CB_GROUP_INDICATION | 0xff01),
    MPDS_PORT_CB_TYPE_DATASET_RECEIVED = (MP_CB_GROUP_MPDS | 0x01)
} MPCallbackType;
#define MPPortCallbackType MPCallbackType // for backward compatibility
#define MP_INDICATION_TYPE_FATAL_ERROR MP_INDICATION_CB_TYPE_FATAL_ERROR // for backward compatibility

#define MP_REQUEST_TYPE_BLOCKING    OS_MESSAGE_BLOCK
#define MP_REQUEST_TYPE_NONBLOCKING OS_MESSAGE_NOBLOCK

// reason code for disconnect
typedef enum
{
    // reason value for disconnect due to external cause.
    MP_DISCONNECT_REASON_RESERVED                      = 0, // reserved
    MP_DISCONNECT_REASON_UNSPECIFIED                   = 1, // error that cannot be specifically identified
    MP_DISCONNECT_REASON_PREV_AUTH_INVALID             = 2, // The immediately previous authentication is no longer valid.
    MP_DISCONNECT_REASON_DEAUTH_LEAVING                = 3, // Deauthenticated because unit has left the BSS.
    MP_DISCONNECT_REASON_INACTIVE                      = 4, // Released connection due to inactivity.
    MP_DISCONNECT_REASON_UNABLE_HANDLE                 = 5, // Released connection due to lack of sufficient resources for AP.
    MP_DISCONNECT_REASON_RX_CLASS2_FROM_NONAUTH_STA    = 6, // A Class2 frame was received from an unauthenticated STA.
    MP_DISCONNECT_REASON_RX_CLASS3_FROM_NONASSOC_STA   = 7, // A Class3 frame was received from an unconnected STA.
    MP_DISCONNECT_REASON_DISASSOC_LEAVING              = 8, // Released association because unit has left the BSS.
    MP_DISCONNECT_REASON_ASSOC_STA_NOTAUTHED           = 9, // An STA which has requested connection has not yet undergone authentication.
    // reason values specific to local communications of the Wii-DS
    MP_DISCONNECT_REASON_NO_ENTRY                     = 19, // Current entry not received for parent.
    // reason value related to auto disconnect from inside the library
    MP_DISCONNECT_REASON_MP_LIFETIME              = 0x8001, // MP communication lifetime has run out.
    MP_DISCONNECT_REASON_UPDATE                   = 0xc001, // Corresponding child has been re-registered under a different AID.
    MP_DISCONNECT_REASON_DEPRIVED_AID             = 0xc002, // Another party in the communication has been registered with the same AID number.

    // reason values for active disconnection by the library
    MP_DISCONNECT_REASON_RESET                    = 0xf001, // disconnected as a result of reset
    MP_DISCONNECT_REASON_ACTIVE                   = 0xf002  // disconnected self using disconnect function
} MPDisconnectReason;
#define MP_IS_DISCONNECTED_FROM_MYSELF(reason)    (((reason)&0xf000) == 0xf000)

#define MP_DEBUG_LEVEL_ERROR            (1U << 0)
#define MP_DEBUG_LEVEL_WARNING          (1U << 1)
#define MP_DEBUG_LEVEL_REPORT           (1U << 2)
#define MP_DEBUG_LEVEL_TRACE            (1U << 8)
#define MP_DEBUG_LEVEL_DEFAULT (MP_DEBUG_LEVEL_ERROR|MP_DEBUG_LEVEL_WARNING|MP_DEBUG_LEVEL_REPORT)

#define MP_DEBUG_LEVEL_DETAIL           (1U << 16)
#define MP_DEBUG_LEVEL_DETAIL_SEND      (1U << 17)
#define MP_DEBUG_LEVEL_DETAIL_RECV      (1U << 18)
#define MP_DEBUG_LEVEL_DETAIL_MPSEQ     (1U << 19)
#define MP_DEBUG_LEVEL_DETAIL_PACKET    (1U << 20)
#define MP_DEBUG_LEVEL_DETAIL_TSF       (1U << 21)
#define MP_DEBUG_LEVEL_DETAIL_TMPTT     (1U << 22)
#define MP_DEBUG_LEVEL_DETAIL_VBLANK    (1U << 23)
#define MP_DEBUG_LEVEL_DETAIL_REQUEST   (1U << 24)
#define MP_DEBUG_LEVEL_DETAIL_IPC       (1U << 25)
#define MP_DEBUG_LEVEL_DETAIL_CALLBACK  (1U << 26)


//////////////////////////////////////////////////////////////////////////////

#if defined(REVO_IOP_ENDIAN_LITTLE)
#define MPMPToH8(data)  (data)
#define MPMPToH16(data) (data)
#define MPMPToH32(data) (data)
#define MPHToMP8(data)  (data)
#define MPHToMP16(data) (data)
#define MPHToMP32(data) (data)
#endif

#if defined(REVO_IOP_ENDIAN_BIG) || defined(GEKKO)
#define MPMPToH8(data)  (u8)(data)
#define MPMPToH16(data) NETSwapBytes16(data)
#define MPMPToH32(data) NETSwapBytes32(data)
#define MPHToMP8(data)  (u8)(data)
#define MPHToMP16(data) NETSwapBytes16(data)
#define MPHToMP32(data) NETSwapBytes32(data)
#endif

//////////////////////////////////////////////////////////////////////////////

#define MP_MACADDRESS_PRINT_FORMAT "%02X:%02X:%02X:%02X:%02X:%02X"
#define MP_MACADDRESS_PRINT_LIST( mac ) \
    (mac)[0], (mac)[1], (mac)[2], (mac)[3], (mac)[4], (mac)[5]


//////////////////////////////////////////////////////////////////////////////

typedef struct MPPortCallbackInfo_Startup
{
    u32 padding;
} MPPortCallbackInfo_Startup;

typedef struct MPPortCallbackInfo_Cleanup
{
    u32 padding;
} MPPortCallbackInfo_Cleanup;

typedef struct MPPortCallbackInfo_DataReceived
{
    u32 fromAid;

    void* data;
    u32 length;
    u32 seqNo;
} MPPortCallbackInfo_DataReceived;


typedef struct MPPortCallbackInfo_Connected
{
    u32 fromAid;

    u32 ssidUserDataLength;

    u8 macAddress[MP_SIZE_MACADDRESS];

    u8 padding[2];

    union {
        struct {
            u8 _ssidInternal[MP_SIZE_SSID_INTERNAL];
            u8 ssidUserData[MP_SIZE_SSID_USER_DATA];
        };
        u8 _ssid[MP_SIZE_SSID];
    };

    u32 _ssidLength;
} MPPortCallbackInfo_Connected;

typedef struct MPPortCallbackInfo_Disconnected
{
    u32 fromAid;

    u32 reason;
    u8 macAddress[6];

    u8 padding[2];
} MPPortCallbackInfo_Disconnected;

typedef struct MPAsyncCallbackInfo_DataSent
{
    u32 aidBits;

    void* data;
    u32 length;

    u32 restBits;

    u32 seqNo;
} MPAsyncCallbackInfo_DataSent;

typedef struct MPAsyncCallbackInfo_BeaconSent
{
    u8 padding[4];
} MPAsyncCallbackInfo_BeaconSent;

typedef struct MPCallbackInfoHeader
{
    s32 type;
    s32 result;
    u32 padding;
    void *userData;
} MPCallbackInfoHeader;

typedef struct MPIndicationInfo
{
    union {
        MPCallbackInfoHeader header;
        // same as MPCallbackInfoHeader
        struct {
            s32 type;
            s32 result;
            s32 value;
            u32 padding;
        };
    };

} MPIndicationInfo;

typedef struct MPCallbackInfo
{
    union {
        MPCallbackInfoHeader header;
        // same as MPCallbackInfoHeader
        struct {
            s32 type;
            s32 result;
            u32 port;
            void *userData;
        };
    };

    union {
        MPPortCallbackInfo_Startup      startup;
        MPPortCallbackInfo_Cleanup      cleanup;
        MPPortCallbackInfo_DataReceived dataReceived;
        MPPortCallbackInfo_Connected    connected;
        MPPortCallbackInfo_Disconnected disconnected;
        MPAsyncCallbackInfo_DataSent    dataSent;
        MPAsyncCallbackInfo_BeaconSent  beaconSent;

        u8  raw_binary[80];
    };
} MPCallbackInfo;
#define MPCommandBlock MPCallbackInfo       // for backward compatibility
#define MPPortCallbackInfo MPCallbackInfo   // for backward compatibility

typedef struct MPChildInfo
{
    u32 aid;
    u8 macAddress[6];
    
    u8 padding[2];
} MPChildInfo;

typedef void (*MPCallback)( s32 result, MPCallbackInfo *info );
typedef void (*MPIndicationCallback)( s32 type, MPIndicationInfo *info );
typedef void (*MPPortCallback)( s32 type, MPPortCallbackInfo *info );

typedef struct MPPortConfig
{
    MPPortCallback  callbackFunction;  // receive callback for port
    u32             priority;          // send priority for port
    void*           userData;          // User data passed to the port receive callback.
    u8              padding[4];
} MPPortConfig;

typedef struct MPConfig
{

    void*       (*alloc)(u32 size);
    void        (*free) (void* ptr);

    OSPriority  threadPriority; // thread priority for MP task

    u32         mode;           // MPMode

    u32         ggid;
    u32         tgid;           // 16-bit ID that must be created each time a parent is started
                                // appropriate value is automatically assigned using MP_TGID_AUTO
    u32         channel;        // channel to be used
                                // appropriate channel is automatically selected using MP_CHANNEL_AUTO

    s32         lifeTime;       // Time limit used to disconnect if state of non-communication continues. Specified in units of milliseconds.
                                // The lifetime feature is disabled if 0 is specified.
                                // Under normal conditions, specify MP_LIFETIME_DEFAULT (4000).

    // for parent mode
    u32         beaconPeriod;   // Beacon interval (ms)
                                // Under normal conditions, specify MP_BEACONPERIOD_AUTO.
    u32         maxNodes;       // number of child devices that can connect
    u32         parentMaxSize;  // maximum parent send size
    u32         childMaxSize;   // maximum child send size
    s32         entryFlag;      // 1: Connect possible, 0: Connect not possible
    s32         multiBootFlag;  // must be set to 0 for normal parents

    s32         frequency;      // MP communication frequency (0 is used to indicate continuous communication)

    u32         userGameInfoLength; // Length of the user area inside GameInfo
    u8          userGameInfo[ MP_GAMEINFO_USER_SIZE_MAX ]; // Data in the user area inside GameInfo

    // Callback
    MPIndicationCallback indicationCallbackFunction;    // callback for notifying indication

    // portConfig
    MPPortConfig portConfig[MP_PORT_MAX];               // receive callback for port

    u8          padding[12];

} MPConfig;


// Same as WDGameInfo
typedef struct MPGameInfo
{
    u16         magicNumber;
    u8          ver; // == 1
    u8          platform;
    u32         ggid;
    u16         tgid;
    u8          userGameInfoLength;     /* byte length of the user area */
    u8          attribute;
    u16         parentMaxSize;
    u16         childMaxSize;
    u8          userGameInfo[ MP_GAMEINFO_USER_SIZE_MAX ];

} __attribute__((packed)) MPGameInfo;


//////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
  Name        : MPStartup
  Description : Initializes and starts the MP communication.
  Arguments   : config  - MP communication settings
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     MPStartup( const MPConfig* config );

/*---------------------------------------------------------------------------*
  Name        : MPCleanup
  Description : Ends the MP communication.
  Arguments   : None.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     MPCleanup( void );

/*---------------------------------------------------------------------------*
  Name        : MPSend
  Description : Sends data.
  Arguments   : data    - Pointer to the data to be sent
                length  - Length of data to be sent
                aidBits - AID list of send destinations
                port    - Port number to which data is to be sent
                restBits - Required resend list of AIDs
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPSend( const void* data, u32 length, u32 aidBits, u32 port, u32* restBits );

/*---------------------------------------------------------------------------*
  Name        : MPSendAsync
  Description : Sends data.
  Arguments   : data    - Pointer to the data to be sent
                length  - Length of data to be sent
                aidBits - AID list of send destinations
                port    - Port number to which data is to be sent
                restBits - Required resend list of AIDs
                cb      - The callback function invoked when an asynchronous function completes.
                userData - Application-dependent pointer value to pass to the callback function.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPSendAsync( const void* data, u32 length, u32 aidBits, u32 port,
                 u32* restBits, MPCallback cb, void* userData );

/*---------------------------------------------------------------------------*
  Name        : MPUpdateBeacon
  Description : Current settings are accessed and GameInfo is updated and sent.
  Arguments   : None.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPUpdateBeacon( void );

/*---------------------------------------------------------------------------*
  Name        : MPUpdateBeaconAsync
  Description : Current settings are accessed and GameInfo is updated and sent.
  Arguments   : cb      - The callback function invoked when an asynchronous function completes.
                userData - Application-dependent pointer value to pass to the callback function.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPUpdateBeaconAsync( MPCallback cb, void* userData );

/*---------------------------------------------------------------------------*
  Name        : MPSetUserGameInfo
  Description : Sets UserGameInfo information.
                To apply the values set using this function to the send beacon, you must call the MPUpdateBeacon function.
                 
  Arguments   : userGameInfo - UserGameInfo information applied to the beacon.
                size - Size of userGameInfo (<= MP_GAMEINFO_USER_SIZE_MAX)
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPSetUserGameInfo( const void* userGameInfo, u32 size );

/*---------------------------------------------------------------------------*
  Name        : MPSetEntryFlag
  Description : Changes connection permission settings.
                To apply the values set using this function to the send beacon, you must call the MPUpdateBeacon function.
                 
  Arguments   : entryFlag - TRUE if child connections are allowed.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPSetEntryFlag( BOOL entryFlag );

s32 MPiSetMultiBootFlag( BOOL multiBootFlag );

/*---------------------------------------------------------------------------*
  Name        : MPGetFatalError
  Description : Returns the type value of the last Fatal Error.
  Arguments   : None.
  Returns     : The type value of the last Fatal Error
 *---------------------------------------------------------------------------*/
s32 MPGetFatalError( void );

/*---------------------------------------------------------------------------*
  Name        : MPSwitchBufferAddrCheck
  Description : Toggles the operation mode: one mode checks whether the buffer used by the MP library exists in MEM2, and the other mode performs no check.
                 
  Arguments   : enabled - Specifies the operation mode for the check.
                            TRUE: Check. FALSE : Don't check.
  Returns     : BOOL - Returns the operation mode from before the switch.
 *---------------------------------------------------------------------------*/
BOOL MPSwitchBufferAddrCheck( BOOL enabled );

/*---------------------------------------------------------------------------*
  Name        : MPDisconnect
  Description : Disconnects the other party in communication.
  Arguments   : aid - AID of the communication target to be disconnected.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPDisconnect( u32 aid );

/*---------------------------------------------------------------------------*
  Name        : MPDisconnectAll
  Description : Disconnects all communicating parties currently connected.
  Arguments   : None.
  Returns     : s32     - Returns the result of the processing. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPDisconnectAll( void );

/*---------------------------------------------------------------------------*
  Name        : MPGetLinkLevel
  Description : Gets the receiving signal strength.
  Arguments   : None.
  Returns     : s32 - Returns a signal strength value from 0 to 3. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32 MPGetLinkLevel( void );

/*---------------------------------------------------------------------------*
  Name        : MPGetConnectedAIDs
  Description : Gets the list of currently connected AIDs.
  Arguments   : None.
  Returns     : u32 value representing set bits corresponding to currently connected AIDs.
 *---------------------------------------------------------------------------*/
u32 MPGetConnectedAIDs( void );

/*---------------------------------------------------------------------------*
  Name        : MPIsConnected
  Description : Determines whether a given AID is currently connected.
  Arguments   : aid - AID to be detected.
  Returns     : TRUE if the AID is connected.
 *---------------------------------------------------------------------------*/
BOOL MPIsConnected( u32 aid );

/*---------------------------------------------------------------------------*
  Name:         MPCountPopulation
  Description:  Determines the number of '1' bits in a binary 32-bit expression.
  Arguments:    x
  Returns:      The number of '1' bits in the binary expression
 *---------------------------------------------------------------------------*/
u8 MPCountPopulation(u32 x);

/*---------------------------------------------------------------------------*
  Name        : MPWaitPseudoVBlank
  Description : Wait for a pseudo V-Blank.
  Arguments   : None.
  Returns     : TRUE if had been waiting for a pseudo V-Blank, FALSE if MP is not in operation.
                 
 *---------------------------------------------------------------------------*/
BOOL MPWaitPseudoVBlank( void );

#ifndef NDEBUG
/*---------------------------------------------------------------------------*
  Name        : MPSetDebugLevel
  Description : Change the level of debug output.
  Arguments   : level - Debug output type bit to be output
  Returns     : None.
 *---------------------------------------------------------------------------*/
void MPSetDebugLevel( u32 level );
#else
#define MPSetDebugLevel( level )    ( (void)0 )
#endif


//////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
  Name        : MPSetIndicationConfig
  Description : Sets the callback for the notifying indication to MPConfig.
                This must be set before calling the MPStartup function.
  Arguments   : config  - MP communication settings
                callback - Callback for indication notification.
  Returns     : None.
 *---------------------------------------------------------------------------*/
static inline void  MPSetIndicationConfig( MPConfig* config, MPIndicationCallback callback )
{
    config->indicationCallbackFunction = callback;
}

/*---------------------------------------------------------------------------*
  Name        : MPSetPortConfig
  Description : Performs the port settings in MPConfig.
                This must be set before calling the MPStartup function.
  Arguments   : config  - MP communication settings
                port     - Port number for which the callback is being set.
                callback - Receive callback for port
                priority - Send priority of the port.
  Returns     : None.
 *---------------------------------------------------------------------------*/
static inline void  MPSetPortConfig( MPConfig* config, u32 port, MPPortCallback callback, u32 priority )
{
    config->portConfig[port].callbackFunction = callback;
    config->portConfig[port].priority = priority;
    config->portConfig[port].userData = NULL;
}

/*---------------------------------------------------------------------------*
  Name        : MPSetPortConfigEx
  Description : Performs the port settings in MPConfig.
                This must be set before calling the MPStartup function.
  Arguments   : config  - MP communication settings
                port     - Port number for which the callback is being set.
                priority - Send priority of the port.
                callback - Receive callback for port
                userData - User data to pass to the port receive callback.
  Returns     : None.
 *---------------------------------------------------------------------------*/
static inline void  MPSetPortConfigEx( MPConfig* config, u32 port, MPPortCallback callback, u32 priority, void* userData )
{
    config->portConfig[port].callbackFunction = callback;
    config->portConfig[port].priority = priority;
    config->portConfig[port].userData = userData;
}

/*---------------------------------------------------------------------------*
  Name        : MPGetCallbackGroup
  Description : Gets the group from the value of the callback type.
  Arguments   : type - callback type
  Returns     : The group to which that callback type belongs.
 *---------------------------------------------------------------------------*/
static inline s32 MPGetCallbackGroup( s32 type )
{
    return (s32)(type & MP_CB_GROUP_MASK);
}




#ifdef MP_RAW_IF

#define MP_NtoHs MPMPToH16
#define MP_NtoHl MPMPToH32
#define MP_HtoNs MPHToMP16
#define MP_HtoNl MPHToMP32

typedef s32 MPError;

MPError MPiRawSendDataToPort(const u16* sendData, u16 sendDataSize, u16 destBitmap, u16 port);
MPError MPiRawReceiveData(u16* recvBuf, u16 recvBufSize);

#define MP_SendDataToPort       MPiRawSendDataToPort
#define MP_ReceiveData          MPiRawReceiveData

#ifdef  REVO_IOP_RELEASE

#   define MP_Panic( ... )      UTL_Terminate()
#   define MP_Warning( ... )    ( (void)0 )

#else

#   define MP_Panic( ... )      UTLi_Panic( "MP", __FILE__, __LINE__, __VA_ARGS__ )
#   define MP_Warning( ... )    UTLi_Warning( "MP", __FILE__, __LINE__, __VA_ARGS__ )

#endif

#endif

/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_MP_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
