/*---------------------------------------------------------------------------*
  Project:  MP DS wireless filesystem library
  File:     mpfs.h

  Copyright 2007 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: mpfs.h,v $
  Revision 1.1  2007/02/09 04:18:26  yosizaki
  Initial upload.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_MPFS_H__
#define REVOLUTION_MPFS_H__

#include <revolution/types.h>
#include <revolution/mp.h>
#include <revolution/mem.h>

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/
/* constants */


/*===========================================================================*/
/* declarations */

/*===========================================================================*/
/* functions */

/*---------------------------------------------------------------------------*
  Name: MPFSInit
  Description: Initializes the MPFS library.
                It is necessary to call this before starting the MP library.
  Arguments   : config           The MPConfig structure that will be run
                                 Management of the MP library is done by the application.
                heap             The heap used internally by the MPFS library
                program          The Nintendo DS program including the file system to be provided to the connection target.
                                 
  Returns: None
 *---------------------------------------------------------------------------*/
void    MPFSInit( MPConfig *config, MEMHeapHandle heap, const void *program );

/*---------------------------------------------------------------------------*
  Name: MPFSEnd
  Description: Terminates the MPFS library
                This must be called after the MP library has terminated.
  Arguments: None
  Returns: None
 *---------------------------------------------------------------------------*/
void    MPFSEnd( void );

/*---------------------------------------------------------------------------*
  Name: MPFSIsAvailable
  Description: Determine whether or not the MPFS library is in use.
  Arguments: None
  Returns: None
 *---------------------------------------------------------------------------*/
BOOL    MPFSIsAvailable( void );

/*---------------------------------------------------------------------------*
  Name: MPFSPortCallback
  Description: Does an MPFS port callback
                This callback must be preconfigured to some port number.
  Arguments: type:       Callback type
                info: Callback information structure
  Returns: None
 *---------------------------------------------------------------------------*/
void    MPFSPortCallback( s32 type, MPPortCallbackInfo* info );


/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_MPFS_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
