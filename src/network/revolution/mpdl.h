/*---------------------------------------------------------------------------*
  Project:  MP DS download play library
  File:     mpdl.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law. They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: mpdl.h,v $
  Revision 1.13  2008/10/30 08:19:04  yosizaki
  Added MPDLResetEntryBitmap.

  Revision 1.12  2008/10/23 06:58:04  yosizaki
  Added MPDLStartDownloadEx.

  Revision 1.11  2008/10/08 02:42:56  yosizaki
  Added entry-filter.

  Revision 1.10  2008/02/01 08:55:34  yosizaki
  Added MPDLSetBootStopper.

  Revision 1.9  2007/10/11 06:39:16  yosizaki
  Support for custom-banner.

  Revision 1.8  2007/04/27 03:44:00  yosizaki
  Added MPDLRegistrationList declaration.

  Revision 1.7  2007/02/16 06:28:35  yosizaki
  Changed type of the playerbits (from int to u32).

  Revision 1.6  2006/08/25 02:05:03  yosizaki
  Added some functions.

  Revision 1.5  2006/08/23 08:55:37  yosizaki
  Added some functions.

  Revision 1.4  2006/08/14 14:41:55  yasu
  Support for PADDING warnings.

  Revision 1.3  2006/07/07 03:12:49  yosizaki
  Added MPDLEnableFlag.

  Revision 1.2  2006/07/05 07:52:27  yosizaki
  Changed type of some members in MPDLConfig.

  Revision 1.1  2006/07/03 23:47:37  yosizaki
  Initial upload.

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_MPDL_H__
#define REVOLUTION_MPDL_H__

#include <revolution/types.h>
#include <revolution/mp.h>

#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/
/* Constants */

#define MPDL_PLAYER_NAME_MAX         10      /* 10 or fewer characters in UTF16-LE */
#define MPDL_DOWNLOAD_PARAMETER_SIZE 32

extern const char *MPDLRegistrationList;


/*===========================================================================*/
/* Declarations */

typedef struct MPDLPlayerInfo
{
    int     aid;
    int     color;
    u16     name_length;
    u16     name[MPDL_PLAYER_NAME_MAX];
    u8      mac[6];
    int     progress;
}
MPDLPlayerInfo;

#pragma warn_padding off
typedef struct MPDLConfig
{
    void*       (*alloc)(u32 size);
    void        (*free) (void* ptr);
    OSPriority  threadPriority;
    u32         ggid;
    u32         tgid;
    u32         channel;

    u8          serverColor;
    
    // 3-byte padding
    
    const u16  *serverName;
    const u16  *programTitle;
    const u16  *programComment;
    u32         programMaxEntry;
    const u8   *programImage;
    const void *bannerCharacter;
    const void *bannerPalette;
    u8          userParam[MPDL_DOWNLOAD_PARAMETER_SIZE];

    void       *internalWork1;
    void       *internalWork2;
    BOOL        entry_flag;
    BOOL        mb_flag;
    u32         bitmap;
    MPConfig    mpConfig[1];
}
MPDLConfig;
#pragma warn_padding reset

typedef BOOL (*MPDLEntryFilterCallback)(const MPDLPlayerInfo*);


/*===========================================================================*/
/* Functions */

/*---------------------------------------------------------------------------*
  Name        : MPDLStartup
  Description : Initializes and starts MP communications for downloading
  Arguments   : config  - Download settings
  Returns     : s32     - Returns the processing results. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     MPDLStartup( MPDLConfig* config );

/*---------------------------------------------------------------------------*
  Name        : MPDLCleanup
  Description : Terminates MP communications for downloading.
  Arguments   : None.
  Returns     : s32     - Returns the processing results. Returns a negative value if processing failed.
 *---------------------------------------------------------------------------*/
s32     MPDLCleanup( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLIsAvailable
  Description : Determines whether the MPDL library process has started and if the library is available.
  Arguments   : None.
  Returns     : BOOL    - TRUE if the MPDLStartup function has already been called.
 *---------------------------------------------------------------------------*/
BOOL    MPDLIsAvailable( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLStartDownloadEx
  Description : Closes download entries and starts the download process.
  Arguments   : bitmap - Bitmap of AID for which entry will be closed.
  Returns     : u32 - Bitmap that shows the states of players who were considered to be entries when download entries were closed.
                          
 *---------------------------------------------------------------------------*/
u32     MPDLStartDownloadEx(u32 bitmap);

/*---------------------------------------------------------------------------*
  Name        : MPDLStartDownload
  Description : Closes all download entries and starts the download process.
  Arguments   : None.
  Returns     : int - Bitmap that shows the states of players who were considered to be entries when download entries were closed.
                          
 *---------------------------------------------------------------------------*/
u32     MPDLStartDownload( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLGetPlayerInfo
  Description : Gets current entry or downloaded player information.
  Arguments   : aid     - Player AID (the parent Wii is 0)
                dst     - Structure that stores player information
  Returns     : BOOL - Returns TRUE and saves data for a valid AID; returns FALSE otherwise.
                          
 *---------------------------------------------------------------------------*/
BOOL    MPDLGetPlayerInfo( int aid, MPDLPlayerInfo* dst );

/*---------------------------------------------------------------------------*
  Name        : MPDLGetEntryBitmap
  Description : Gets the current entry player status as a bitmap.
  Arguments   : None.
  Returns     : int     - Bitmap representing current entry player status.
 *---------------------------------------------------------------------------*/
u32     MPDLGetEntryBitmap( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLGetBootableBitmap
  Description : Gets download-completed player status as a bitmap.
  Arguments   : None.
  Returns     : u32     - Bitmap representing players who have completed downloading.
 *---------------------------------------------------------------------------*/
u32     MPDLGetBootableBitmap( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLGetBootedBitmap
  Description : Gets a bitmap showing the statuses of players who have completed restarting.
  Arguments   : None.
  Returns     : int     - Bitmap representing players who have completed restarting.
 *---------------------------------------------------------------------------*/
u32     MPDLGetBootedBitmap( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLGetBootStopper
  Description : Gets the states of booting permissions for players who have have completed downloading.
  Arguments   : None.
  Returns     : AID bitmap of players who are not allowed to start.
 *---------------------------------------------------------------------------*/
u32     MPDLGetBootStopper( void );

/*---------------------------------------------------------------------------*
  Name        : MPDLSetBootStopper
  Description : Changes permissions for players to restart if they have completed downloading.
  Arguments   : bitmap  - AID bitmap of players who are not allowed to start.
  Returns     : None.
 *---------------------------------------------------------------------------*/
void    MPDLSetBootStopper( u16 bitmap );

/*---------------------------------------------------------------------------*
  Name        : MPDLResetEntryBitmap
  Description : Reaccepts entries for AIDs that are currently in an open state.
                This is used to start the next download entry for AIDs that completed downloading or were disconnected while downloading.
                
  Arguments   : bitmap  - AID bitmap of players whose entries will be re-allowed.
                          AIDs that are currently connected will be ignored.
  Returns     : Bitmap of AIDs that were actually accepted and restarted.
 *---------------------------------------------------------------------------*/
u32     MPDLResetEntryBitmap(u32 bitmap);

/*---------------------------------------------------------------------------*
  Name        : MPDLSetEntryFilterCallback
  Description : Sets an entry permission callback to be invoked for players immediately after they connect.
  Arguments   : func - Entry permission callback (an MPDLEntryFilterCallback type).
                          Specifying NULL will cause entries to always be allowed.
  Returns     : None.
 *---------------------------------------------------------------------------*/
void MPDLSetEntryFilterCallback(MPDLEntryFilterCallback func);


/* internal functions -------------------------------------------------------*/

/*---------------------------------------------------------------------------*
  Name        : MPDLEnableFlag
  Description : Controls the communication beacon flag.
  Arguments   : entry - Controls entry flag (normally, always true)
                mb - Controls MB flag (normally, always true)
  Returns     : None.
 *---------------------------------------------------------------------------*/
void    MPDLEnableFlag( BOOL entry, BOOL mb );


/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_MP_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
