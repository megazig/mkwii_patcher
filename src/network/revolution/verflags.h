#if 0
#============================================================================
# Project:  RevolutionSDK Extensions - include
# File:     verflags.h
#
# Copyright 2006 Nintendo.  All rights reserved.
#
# These coded instructions, statements, and computer programs contain
# proprietary information of Nintendo of America Inc. and/or Nintendo
# Company Ltd., and are protected by Federal copyright law.  They may
# not be disclosed to third parties or copied or duplicated in any form,
# in whole or in part, without the prior written consent of Nintendo.
#============================================================================
# Flags for workaround
#   - This file is generated automatically.
#   - This file can be included in C/C++ and/or Makefile script.
#
#endif

#ifndef REX_REVOLUTION_VERFLAGS_H_
#define REX_REVOLUTION_VERFLAGS_H_

#endif//REX_REVOLUTION_VERFLAGS_H_
