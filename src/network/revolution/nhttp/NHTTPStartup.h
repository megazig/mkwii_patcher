/*---------------------------------------------------------------------------*
  Project:  Revolution NHTTP library
  File:     NHTTPStartup.h

  Copyright (C) 2008 Nintendo All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef __NHTTP_STARTUP_H__
#define __NHTTP_STARTUP_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*
 * Includes
 *---------------------------------------------------------------------------*/
#if defined(RVL_OS)
#include <revolution.h>
#endif  // RVL_OS

/*---------------------------------------------------------------------------*
 * Types/Declarations
 *---------------------------------------------------------------------------*/
typedef enum {
    NHTTP_ERROR_SYSTEM = -1,    
     
    NHTTP_ERROR_NONE = 0,       // 0
    NHTTP_ERROR_ALLOC,          // 1
    NHTTP_ERROR_TOOMANYREQ,     // 2
    NHTTP_ERROR_SOCKET,         // 3
    NHTTP_ERROR_DNS,            // 4
    NHTTP_ERROR_CONNECT,        // 5
    NHTTP_ERROR_BUFFULL,        // 6
    NHTTP_ERROR_HTTPPARSE,      // 7
    NHTTP_ERROR_CANCELED,       // 8
    NHTTP_ERROR_REVOLUTIONSDK,  // 9
    NHTTP_ERROR_REVOLUTIONWIFI, // 10
    NHTTP_ERROR_UNKNOWN,        // 11
    NHTTP_ERROR_DNS_PROXY,      // 12
    NHTTP_ERROR_CONNECT_PROXY,  // 13
    NHTTP_ERROR_SSL,            // 14
    NHTTP_ERROR_BUSY,           // 15
    NHTTP_ERROR_SSL_ROOTCA,     // 16
    NHTTP_ERROR_SSL_CLIENTCERT, // 17
    
    NHTTP_ERROR_MAX
} NHTTPError;

typedef void* (*NHTTPAlloc)          ( u32 size, int align );
typedef void  (*NHTTPFree)           ( void* ptr );
typedef void  (*NHTTPCleanupCallback)( void );

/*---------------------------------------------------------------------------*
 * Function Prototypes
 *---------------------------------------------------------------------------*/
extern int                      NHTTPStartup              ( NHTTPAlloc alloc, NHTTPFree free, u32 threadprio );
extern void                     NHTTPCleanupAsync         ( NHTTPCleanupCallback callback );
extern NHTTPError               NHTTPGetError             ( void );



#ifdef __cplusplus
}
#endif

#endif  // __NHTTP_STARTUP_H__
