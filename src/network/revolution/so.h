/*---------------------------------------------------------------------------*
  Project:  SO library
  File:     so.h

  Copyright 2006 Nintendo.  All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: so.h,v $
  Revision 1.8  2007/08/10 12:30:59  seiki_masashi
  Changed SO_STARTUP_TIMEOUT from 600 seconds to 60 seconds.

  Revision 1.7  2007/01/29 07:47:21  terui
  Definitions relating to public functions in the SO library were moved to SOBasic.h.
  Settings relating to IPv6 were moved to SOPrivate.h.
  include header declarations were moved to SOPrivate.h.

  Revision 1.6  2006/09/15 06:31:09  terui
  Switched IPv6 support

  Revision 1.5  2006/09/14 07:42:43  yasu
  IP4 support

  Revision 1.4  2006/08/08 14:00:31  terui
  Fixed comments.

  Revision 1.3  2006/08/08 13:50:41  terui
  Divided and set up SOBasic.h, SOIcmp.h and SOOption.h

  Revision 1.2  2006/08/08 06:46:50  terui
  Provisional version during porting

  Revision 1.1  2006/08/07 02:16:30  terui
  Uploaded the current state

  $NoKeywords: $
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_SO_H_
#define REVOLUTION_SO_H_

#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

/*---------------------------------------------------------------------------*
    Fixed numbers
 *---------------------------------------------------------------------------*/

/* Result codes */
#define     SO_SUCCESS              0
#define     SO_OK                   SO_SUCCESS
#define     SO_EFATAL               0x80000000
#define     SO_E2BIG                (-1)
#define     SO_EACCES               (-2)
#define     SO_EADDRINUSE           (-3)            // Address is already in use
#define     SO_EADDRNOTAVAIL        (-4)
#define     SO_EAFNOSUPPORT         (-5)            // Non-supported address family
#define     SO_EAGAIN               (-6)            // Posix.1
#define     SO_EALREADY             (-7)            // Already in progress
#define     SO_EBADF                (-8)            // Bad socket descriptor
#define     SO_EBADMSG              (-9)
#define     SO_EBUSY                (-10)           // Busy
#define     SO_ECANCELED            (-11)
#define     SO_ECHILD               (-12)
#define     SO_ECONNABORTED         (-13)           // Connection aborted
#define     SO_ECONNREFUSED         (-14)           // Connection refused
#define     SO_ECONNRESET           (-15)           // Connection reset
#define     SO_EDEADLK              (-16)
#define     SO_EDESTADDRREQ         (-17)           // Not bound to a local address
#define     SO_EDOM                 (-18)
#define     SO_EDQUOT               (-19)
#define     SO_EEXIST               (-20)
#define     SO_EFAULT               (-21)
#define     SO_EFBIG                (-22)
#define     SO_EHOSTUNREACH         (-23)
#define     SO_EIDRM                (-24)
#define     SO_EILSEQ               (-25)
#define     SO_EINPROGRESS          (-26)           // Still in progress
#define     SO_EINTR                (-27)           // Canceled
#define     SO_EINVAL               (-28)           // Invalid operation
#define     SO_EIO                  (-29)           // I/O error
#define     SO_EISCONN              (-30)           // Socket is already connected
#define     SO_EISDIR               (-31)
#define     SO_ELOOP                (-32)
#define     SO_EMFILE               (-33)           // No more socket descriptors
#define     SO_EMLINK               (-34)
#define     SO_EMSGSIZE             (-35)           // Too large to be sent
#define     SO_EMULTIHOP            (-36)
#define     SO_ENAMETOOLONG         (-37)
#define     SO_ENETDOWN             (-38)
#define     SO_ENETRESET            (-39)
#define     SO_ENETUNREACH          (-40)           // Unreachable
#define     SO_ENFILE               (-41)
#define     SO_ENOBUFS              (-42)           // Insufficient resources
#define     SO_ENODATA              (-43)
#define     SO_ENODEV               (-44)
#define     SO_ENOENT               (-45)
#define     SO_ENOEXEC              (-46)
#define     SO_ENOLCK               (-47)
#define     SO_ENOLINK              (-48)           // Network interface is unavailable
#define     SO_ENOMEM               (-49)           // Insufficient memory
#define     SO_ENOMSG               (-50)
#define     SO_ENOPROTOOPT          (-51)           // Non-supported option
#define     SO_ENOSPC               (-52)
#define     SO_ENOSR                (-53)
#define     SO_ENOSTR               (-54)
#define     SO_ENOSYS               (-55)
#define     SO_ENOTCONN             (-56)           // Not connected
#define     SO_ENOTDIR              (-57)
#define     SO_ENOTEMPTY            (-58)
#define     SO_ENOTSOCK             (-59)           // Not a socket
#define     SO_ENOTSUP              (-60)
#define     SO_ENOTTY               (-61)
#define     SO_ENXIO                (-62)           // Network interface device doesn't exist
#define     SO_EOPNOTSUPP           (-63)           // Non-supported operation
#define     SO_EOVERFLOW            (-64)
#define     SO_EPERM                (-65)
#define     SO_EPIPE                (-66)
#define     SO_EPROTO               (-67)
#define     SO_EPROTONOSUPPORT      (-68)           // Non-supported protocol
#define     SO_EPROTOTYPE           (-69)           // Non-supported socket type
#define     SO_ERANGE               (-70)
#define     SO_EROFS                (-71)
#define     SO_ESPIPE               (-72)
#define     SO_ESRCH                (-73)
#define     SO_ESTALE               (-74)
#define     SO_ETIME                (-75)
#define     SO_ETIMEDOUT            (-76)           // Timed out
#define     SO_ETXTBSY              (-77)
#define     SO_EWOULDBLOCK          SO_EAGAIN       // Posix.1g
#define     SO_EXDEV                (-78)
/* Host configuration error codes */
#define     SO_ERR_DHCP_TIMEOUT     (-100)          // Could not find any DHCP server.
#define     SO_ERR_DHCP_EXPIRED     (-101)
#define     SO_ERR_DHCP_NAK         (-102)
    /* -103 to -106 are reserved */
#define     SO_ERR_LCP              (-107)          // LCP negotiation error
#define     SO_ERR_AUTH             (-108)          // Authentication error
#define     SO_ERR_IPCP             (-109)          // IPCP negotiation error
    /* -110 is reserved */
#define     SO_ERR_ADDR_COLLISION   (-111)          // Duplicate IP address
#define     SO_ERR_LINK_DOWN        (-112)          // Link down
/* Connection error codes */
#define     SO_ERR_LINK_UP_TIMEOUT  (-121)          // Link-up timeout

/* Name for allocator */
    /* 0 to 9 are reserved */
#define     SO_MEM_ADDRINFO         10
#define     SO_MEM_RESOLVER         11
#define     SO_MEM_IPC              12
#define     SO_MEM_RECVFROM         13
#define     SO_MEM_SENDTO           14

/* SOStartup() timeout default settings */
#define SO_STARTUP_TIMEOUT          60000

/* Retry count of link-up */
#define SO_STARTUP_LINK_UP_RETRY_COUNT  4


/*---------------------------------------------------------------------------*
    Structures
 *---------------------------------------------------------------------------*/

/* Allocator type definition */
typedef void*   ( *SOAlloc )    ( u32 name, s32 size );
typedef void    ( *SOFree )     ( u32 name, void* ptr, s32 size );
/*
 * Notice !
 * Allocator must supply 32-byte aligned buffer.
 */

/* Config of SOInit() */
typedef struct SOLibraryConfig
{
    SOAlloc     alloc;
    SOFree      free;

} SOLibraryConfig;


/*---------------------------------------------------------------------------*
    Functions
 *---------------------------------------------------------------------------*/

int     SOInit      ( SOLibraryConfig* config );
int     SOFinish    ( void );
int     SOStartup   ( void );
int     SOStartupEx ( int timeOut );
int     SOCleanup   ( void );


/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif
#endif  /* REVOLUTION_SO_H_ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
