/*---------------------------------------------------------------------------*
  Project:  NCD library
  File:     NCDMisc.h

  Copyright 2006,2007 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

  $Log: NCDMisc.h,v $
  Revision 1.4  2007/01/31 02:32:26  okubata_ryoma
  NCDGetWirelessMacAddress->NETGetWirelessMacAddress

  Revision 1.3  2006/10/16 10:46:39  seiki_masashi
  NCDDisableNetworkInterface

  Revision 1.2  2006/08/24 08:17:58  terui
  Added definition of MAC address length.

  Revision 1.1  2006/08/24 08:09:21  yasu
  Split up functions for getting wireless MAC addresses in the console.

  Revision 1.3  2006/08/11 08:34:28  yasu
  The constant in nwc24cntl.h is returned to NCDConst.h

  Revision 1.2  2006/08/11 01:07:20  yasu
  Support for process/kd

  Revision 1.1  2006/08/10 08:11:22  yasu
  Created nwc24cntl.h

  $NoKeywords: $
 *---------------------------------------------------------------------------*/
#ifndef REVOLUTION_NCD_NCDMISC_H__
#define REVOLUTION_NCD_NCDMISC_H__

#ifdef RVL_OS
#include <revolution/types.h>
#include <revolution/ncd/NCDConst.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define NCD_MACADDRESS_LENGTH   6

#ifdef RVL_OS
/*---------------------------------------------------------------------------*
  Name: NCDSetNwc24Permission
  Description: Changes the NWC24 permission settings.
                This does not get reflected in NAND.
  Arguments: permission  - The permission setting. (NCD_NWC24_PERMISSION_*  result of OR operation)
  Returns: s32     - Returns the result of processing. Returns a negative number if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDSetNwc24Permission( u8 permission );

/*---------------------------------------------------------------------------*
  Name: NCDiGetWirelessMacAddress
  Description: Gets the MAC address of the wireless LAN module.
  Arguments: pAddr   - Specifies the buffer for reading out the MAC address.
  Returns: s32     - Returns the result of processing. Returns a negative number if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDiGetWirelessMacAddress( u8* pAddr );

/*---------------------------------------------------------------------------*
  Name: NCDDisableNetworkInterface
  Description: Disables the network interface.
  Arguments: None
  Returns: s32     - Returns the result of processing. Returns a negative number if processing failed.
 *---------------------------------------------------------------------------*/
s32     NCDDisableNetworkInterface( void );


#endif

#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_NCD_H__ */

/*---------------------------------------------------------------------------*
  End of file
 *---------------------------------------------------------------------------*/
