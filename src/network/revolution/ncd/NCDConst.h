/*---------------------------------------------------------------------------*
  Project:  NCD library
  File:     NCDConst.h

  Copyright 2006,2007 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.

 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_NCD_NCDCONST_H__
#define REVOLUTION_NCD_NCDCONST_H__
#ifdef __cplusplus
extern "C" {
#endif
/*===========================================================================*/

/* PPPoE is not-supported */
#undef  NCD_PPPOE_SUPPORT

/* Wired LAN fixed link type is non-supported */
#undef  NCD_WIRED_LINKTYPE_SUPPORT

/*---------------------------------------------------------------------------*/

/* Processing result */
#define     NCD_RESULT_SUCCESS              0       /* Succeeded */
#define     NCD_RESULT_FATAL_ERROR          (-1)    /* This doesn't usually occur. When dealing with bugs related to corrupted memory */
#define     NCD_RESULT_FAILURE              (-2)    /* Failed to operate resource manager */
#define     NCD_RESULT_ILLEGAL_PARAM        (-3)    /* Invalid parameter */
#define     NCD_RESULT_EXCLUSIVE            (-4)    /* Currently being exclusively controlled */
#define     NCD_RESULT_ILLEGAL_STATUS       (-5)    /* Status in which APIs cannot be called */
#define     NCD_RESULT_SFFS_ERROR           (-6)    /* Failed to flush NAND */
#define     NCD_RESULT_UNDECIDED            (-7)    /* Network interface not set */
#define     NCD_RESULT_INPROGRESS           (-8)    /* Preparations for firmware not ready */

/* NCD process status ID */
#define     NCD_LINKSTATUS_WORKING          1       /* Automatic state transition is in mid-transition */
#define     NCD_LINKSTATUS_NONE             2       /* Device to be linked not configured */
#define     NCD_LINKSTATUS_WIRED            3       /* Link with wired device as target */
#define     NCD_LINKSTATUS_WIRELESS_DOWN    4       /* Status in which link cannot be made with target wireless device */
#define     NCD_LINKSTATUS_WIRELESS_UP      5       /* Status in which the link with the target wireless device is established */

/*---------------------------------------------------------------------------*/

#define     NCD_CONFIG_VERSION              0       /* Version information for NCDConfig structure */

#define     NCD_SIZE_SSID                   32
#define     NCD_USBAP_NICKNAME_LENGTH       11
#define     NCD_SIZE_IP                     4

#ifdef  NCD_PPPOE_SUPPORT
#define     NCD_PPPOE_SERVICENAME_LENGTH    128
#define     NCD_PPPOE_USERNAME_LENGTH       256
#define     NCD_PPPOE_PASSWORD_LENGTH       256
#endif

#ifdef  NCD_WIRED_LINKTYPE_SUPPORT
typedef enum
{
    NCD_WIRED_LINKTYPE_AUTO             =   0x00,
    NCD_WIRED_LINKTYPE_10BASE_T         =   0x01,
    NCD_WIRED_LINKTYPE_100BASE_TX       =   0x02

} NCDWiredLinkType;
#endif

typedef enum
{
    NCD_PRIVACY_MODE_NONE               =   0x0000, /* No encryption */
    NCD_PRIVACY_MODE_WEP40              =   0x0001, /* WEP (RC4 40 bit) encryption */
    NCD_PRIVACY_MODE_WEP104             =   0x0002, /* WEP (RC4 104 bit) encryption */
    NCD_PRIVACY_MODE_WPA_PSK_TKIP       =   0x0004, /* WPA-PSK (TKIP) encryption */
    NCD_PRIVACY_MODE_WPA2_PSK_AES       =   0x0005, /* WPA2-PSK (AES) encryption */
    NCD_PRIVACY_MODE_WPA_PSK_AES        =   0x0006  /* WPA-PSK (AES) encryption */

} NCDPrivacyMode;

typedef enum
{
    NCD_CONFIG_METHOD_MANUAL            =   0x00,   /* Manual setting */
    NCD_CONFIG_METHOD_USBAP             =   0x01,   /* Connection settings with USB-AP */
    NCD_CONFIG_METHOD_AOSS              =   0x02,   /* Semi-automatic setting using AOSS */
    NCD_CONFIG_METHOD_RAKURAKU          =   0x03,   /* Semi-automatic setting using easy wireless start */
    NCD_CONFIG_METHOD_SIMPLECONFIG      =   0x04    /* [TODO] Undecided whether or not to support */

} NCDConfMethod;

#define     NCD_FLAG_IS_WIRED               0x01
#define     NCD_FLAG_USE_DHCP_ADDR          0x02
#define     NCD_FLAG_USE_DHCP_DNS           0x04    /* [TODO] Undecided whether or not to support */
#ifdef  NCD_PPPOE_SUPPORT
#define     NCD_FLAG_USE_PPPOE              0x08
#endif
#define     NCD_FLAG_USE_PROXY              0x10
#define     NCD_FLAG_PASSED_CONNECTION_TEST 0x20    /* Connection test pass/fail */
#define     NCD_FLAG_USE_PMTU_RECOVER       0x40    /* [TODO] Undecided whether or not to support */
#define     NCD_FLAG_ENABLE                 0x80

#define     NCD_NWC24_PERMISSION_FORBIDDEN  0x00
#define     NCD_NWC24_PERMISSION_SEND_MAIL  0x01
#define     NCD_NWC24_PERMISSION_RECV_MAIL  0x02
#define     NCD_NWC24_PERMISSION_DOWNLOAD   0x04
#define     NCD_NWC24_PERMISSION_ALL        ((u8)( NCD_NWC24_PERMISSION_SEND_MAIL | NCD_NWC24_PERMISSION_RECV_MAIL | NCD_NWC24_PERMISSION_DOWNLOAD ))

#define     NCD_PROXYNAME_LENGTH            256
#define     NCD_PROXYNAME_LENGTH_MIN        1
#define     NCD_PROXY_USERNAME_LENGTH       33
#define     NCD_PROXY_PASSWORD_LENGTH       33
#define     NCD_NO_PROXY_LENGTH             1024
#define     NCD_AUTOMATIC_PROXY_LENGTH      256

typedef enum
{
    NCD_PROXY_MODE_NONE                =    0x00,  /* Do not use proxy */
    NCD_PROXY_MODE_NORMAL              =    0x01   /* Normal proxy */
} NCDProxyMode;

typedef enum
{
    NCD_PROXY_AUTHTYPE_NONE            =    0x00,  /* No authentication */
    NCD_PROXY_AUTHTYPE_BASIC           =    0x01   /* BASIC authentication */
} NCDProxyAuthType;

#define     NCD_IF_SELECT_NONE              0x00
#define     NCD_IF_SELECT_WIRELESS          0x01
#define     NCD_IF_SELECT_WIRED             0x02
#define     NCD_PROFILE_COUNT               3


/*===========================================================================*/
#ifdef __cplusplus
}
#endif
#endif  /* REVOLUTION_NCD_NCDCONST_H__ */

/*---------------------------------------------------------------------------*
  $Log: NCDConst.h,v $
  Revision 1.16  2007/05/01 04:50:57  adachi_hiroaki
  Changed NCD_PROXYNAME_LENGTH_MIN to 1

  Revision 1.15  2006/09/20 11:22:05  terui
  Added NCD_FLAG_PASSED_CONNECTION_TEST definition

  Revision 1.14  2006/09/02 08:17:20  yoshioka_yasuhiro
  Implemented warning measures.

  Revision 1.13  2006/09/02 05:14:18  seiki_masashi
  Changed the NCDProxyServerProfile structure

  Revision 1.12  2006/08/31 13:24:39  terui
  Support for addition of wireless encryption system WPA-PSK (AES)

  Revision 1.11  2006/08/30 12:16:42  adachi_hiroaki
  Added definitions related to the proxy server

  Revision 1.10  2006/08/18 02:42:55  adachi_hiroaki
  Revised definition of NCD_NWC24_PERMISSION_ALL.

  Revision 1.9  2006/08/16 04:01:08  seiki_masashi
  Changed the size of the user name and password for proxy authentication from 32 to 33

  Revision 1.8  2006/08/16 03:38:06  seiki_masashi
  Added a user name and password to the Proxy settings
  Added version information to NCDConfig

  Revision 1.7  2006/08/11 08:34:28  yasu
  Returned constants in nwc24cntl.h to NCDConst.h

  Revision 1.6  2006/08/11 01:46:30  terui
  Handled wired LAN fixed link type non-support

  Revision 1.5  2006/08/10 12:55:53  terui
  Added definition of NCD_RESULT_INPROGRESS

  Revision 1.4  2006/08/10 08:11:22  yasu
  Created nwc24cntl.h

  Revision 1.3  2006/08/03 09:29:51  seiki_masashi
  typedef added to enum

  Revision 1.2  2006/07/29 04:17:58  terui
  Added constant definitions for enabling NWC24.

  Revision 1.1  2006/07/26 07:24:17  terui
  Moves from posrts/shared

  Revision 1.2  2006/07/22 12:00:00  terui
  Moved constant definitions from NCDConst.h.
  Moved IPC command ID definition and resource manager path string definition to NCDPrivate.h.
  Fixed comments.

  Revision 1.1  2006/06/16 07:32:44  terui
  Moved ncd/ncdshared.h

 *---------------------------------------------------------------------------*/
