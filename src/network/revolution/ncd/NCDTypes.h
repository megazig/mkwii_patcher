/*---------------------------------------------------------------------------*
  Project:  NCD library
  File:     NCDTypes.h

  Copyright 2006 Nintendo. All rights reserved.

  These coded instructions, statements, and computer programs contain
  proprietary information of Nintendo of America Inc. and/or Nintendo
  Company Ltd., and are protected by Federal copyright law.  They may
  not be disclosed to third parties or copied or duplicated in any form,
  in whole or in part, without the prior written consent of Nintendo.
 *---------------------------------------------------------------------------*/

#ifndef REVOLUTION_NCD_NCDTYPES_H_
#define REVOLUTION_NCD_NCDTYPES_H_
#ifdef  __cplusplus
extern "C" {
#endif
/*===========================================================================*/

#define     ATTRIBUTE_PACKED    __attribute__((packed))

#include <revolution/ncd/NCDConst.h>

/* Privacy (wireless encryption) structure */
typedef struct NCDPrivacy
{
    u16     mode;               /* NCDPrivacyMode */
    u8      reserved[ 2 ];
    union
    {
        struct
        {
            u16     keyId;
#ifdef  RVL_OS
            union
            {
                u16     option;
                u8      reserved[ 2 ];
            };
#else
            u8      reserved[ 2 ];
#endif
            u8      key[ 4 ][ 5 ];
        
        } wep40;
        struct
        {
            u16     keyId;
#ifdef  RVL_OS
            union
            {
                u16     option;
                u8      reserved[ 2 ];
            };
#else
            u8      reserved[ 2 ];
#endif
            u8      key[ 4 ][ 13 ];
        
        } wep104;
        struct
        {
            u16     keyLen;
            u8      reserved[ 2 ];
            u8      key[ 64 ];
        
        } tkip;
        struct
        {
            u16     keyLen;
            u8      reserved[ 2 ];
            u8      key[ 64 ];
        
        } aes;
    
    };

} ATTRIBUTE_PACKED NCDPrivacy;

/* AOSS-based connection settings with AP */
typedef struct NCDAossConfig
{
    struct
    {
        u8      ssid[ NCD_SIZE_SSID ];
        u16     ssidLength;
        u16     keyId;
        u8      key[ 4 ][ 5 ];
    
    } wep40;
    struct
    {
        u8      ssid[ NCD_SIZE_SSID ];
        u16     ssidLength;
        u16     keyId;
        u8      key[ 4 ][ 13 ];
    
    } wep104;
    struct
    {
        u8      ssid[ NCD_SIZE_SSID ];
        u16     ssidLength;
        u16     keyLen;
        u8      key[ 64 ];
    
    } tkip;
    struct
    {
        u8      ssid[ NCD_SIZE_SSID ];
        u16     ssidLength;
        u16     keyLen;
        u8      key[ 64 ];
    
    } aes;

} ATTRIBUTE_PACKED NCDAossConfig;

/* Connection settings with standard AP */
typedef struct NCDApConfig
{
    u8              ssid[ NCD_SIZE_SSID ];
    u16             ssidLength;
    u8              reserved[ 2 ];
    NCDPrivacy      privacy;

} ATTRIBUTE_PACKED NCDApConfig;

/* Connection settings with USB-AP */
typedef struct NCDUsbapConfig
{
    u16     nickname[ NCD_USBAP_NICKNAME_LENGTH ];
    u8      reserved[ 2 ];

} ATTRIBUTE_PACKED NCDUsbapConfig;

/* Wired profile structure */
typedef struct NCDWiredProfile
{
#ifdef  NCD_WIRED_LINKTYPE_SUPPORT
    u8      linkType;           /* NCD_WIRED_LINKTYPE_* */
    u8      reserved[ 3 ];
#else
    u8      reserved[ 4 ];
#endif

} ATTRIBUTE_PACKED NCDWiredProfile;

/* Wireless profile structure */
typedef struct NCDWirelessProfile
{
    u16         rateset;        /* Communications rate bit field (0x0001: 1M, 0x0800: 54M, 0x0000: Automatic) */
    u8          configMethod;   /* NCD_CONFIG_METHOD_* */
    u8          retryLimit;     /* Send retry limit (0: Automatic) */
    union
    {
        NCDApConfig         manual;
        NCDUsbapConfig      usbap;
        NCDAossConfig       aoss;
        NCDApConfig         rakuraku;
    } config;

} ATTRIBUTE_PACKED NCDWirelessProfile;

/* Socket library adjustment parameter structure */
typedef struct NCDIpAdjustProfile
{
    s32     maxTransferUnit;        /* default (1500) */
    s32     tcpRetransTimeout;      /* default (100) TCP timeout (unit: seconds) */
    s32     dhcpRetransCount;       /* default (4) DHCP Retry count */

} ATTRIBUTE_PACKED NCDIpAdjustProfile;

/* Internet protocol settings structure */
typedef struct NCDIpProfile
{
    u8      addr[ NCD_SIZE_IP ];    /* IP address */
    u8      netmask[ NCD_SIZE_IP ]; /* Sub-net mask */
    u8      gateway[ NCD_SIZE_IP ]; /* IP address of the gateway */
    u8      dns1[ NCD_SIZE_IP ];    /* IP address of the primary DNS server */
    u8      dns2[ NCD_SIZE_IP ];    /* IP address of the secondary DNS server */

} ATTRIBUTE_PACKED NCDIpProfile;

/* Proxy server settings structure */
typedef struct NCDProxyServerProfile
{
    u8      mode;                           /* Proxy type NCD_PROXY_MODE_* */
    u8      authType;                       /* Proxy authentication type NCD_PROXY_AUTHTYPE_* */
    u8      reserved[ 2 ];
    s8      server[ NCD_PROXYNAME_LENGTH ]; /* Proxy server name */
    u16     port;                           /* Proxy port number */
    s8      username[ NCD_PROXY_USERNAME_LENGTH ];  /* Proxy authentication user name */
    s8      password[ NCD_PROXY_PASSWORD_LENGTH ];  /* Proxy authentication password */

} ATTRIBUTE_PACKED NCDProxyServerProfile;

/* Proxy profile structure */
typedef struct NCDProxyProfile
{
    NCDProxyServerProfile   http;   /* Set HTTP proxy server */
    NCDProxyServerProfile   ssl;    /* Set SSL (HTTPS) proxy server */

    /* Below, unused area */
    struct
    {
        u32     isEnabled;                      /* Whether this setting value is valid */
        s8      servers[ NCD_NO_PROXY_LENGTH ]; /* List of server names, separated by spaces, without proxy connections */
    
    } noProxy;

    struct
    {
        u32     isEnabled1_1;
        u32     isAutomaticEnabled;
        s8      automatic[ NCD_AUTOMATIC_PROXY_LENGTH ];
    
    } browser;

} ATTRIBUTE_PACKED NCDProxyProfile;

/* PPPoE settings structure */
#ifdef  NCD_PPPOE_SUPPORT
typedef struct NCDPppoeProfile
{
    u8      servicename[ NCD_PPPOE_SERVICENAME_LENGTH ];
    u8      username[ NCD_PPPOE_USERNAME_LENGTH ];
    u8      password[ NCD_PPPOE_PASSWORD_LENGTH ];

} ATTRIBUTE_PACKED NCDPppoeProfile;
#endif

/* Overall profile structure for network */
typedef struct NCDProfile
{
    u8      flags;          /* NCD_FLAG_* result of OR operation */
    u8      reserved[ 3 ];

    NCDIpProfile        ip;
    NCDIpAdjustProfile  adjust;
    NCDProxyProfile     proxy;

#ifdef  NCD_PPPOE_SUPPORT
    NCDPppoeProfile     pppoe;
#endif

    union
    {
        NCDWiredProfile      wired;
        NCDWirelessProfile   wireless;
    
    } netif;

} ATTRIBUTE_PACKED NCDProfile;

/* Overall network configuration structure */
typedef struct NCDConfig
{
    u32             version;            /* Set NCD_CONFIG_VERSION */
    u8              selectedMedia;      /* NCD_IF_SELECT_* */
    u8              nwc24Permission;    /* NCD_NWC24_PERMISSION_* result of OR operation */
    u8              linkTimeout;        /* Units of seconds */
    u8              reserved[ 1 ];
    NCDProfile      profiles[ NCD_PROFILE_COUNT ];

} ATTRIBUTE_PACKED NCDConfig;

/*---------------------------------------------------------------------------*/

#ifdef RVL_OS

/* Network interface configuration structure */
typedef struct NCDIfConfig
{
    u8      selectedMedia;      /* NCD_IF_SELECT_* */
    u8      linkTimeout;
    union
    {
        NCDWiredProfile     wired;
        NCDWirelessProfile  wireless;
    
    } netif;

} NCDIfConfig;

/* TCP/IP-related configuration structure */
typedef struct NCDIpConfig
{
    BOOL        useDhcp;
    BOOL        useProxy;
#ifdef  NCD_PPPOE_SUPPORT
    BOOL        usePppoe;
#endif

    NCDIpProfile        ip;
    NCDIpAdjustProfile  adjust;
    NCDProxyProfile     proxy;

#ifdef  NCD_PPPOE_SUPPORT
    NCDPppoeProfile     pppoe;
#endif

} NCDIpConfig;

#endif


/*===========================================================================*/
#ifdef  __cplusplus
}       /* extern "C" */
#endif
#endif  /* REVOLUTION_NCD_NCDTYPES_H_ */

/*---------------------------------------------------------------------------*
  $Log: NCDTypes.h,v $
  Revision 1.15  2006/10/16 06:05:17  terui
  Revisions to match the difference in padding from the compiler.

  Revision 1.14  2006/10/12 04:29:55  terui
  Simplified the definition of the field storing the WEP key input state.

  Revision 1.13  2006/10/12 00:19:34  terui
  Added definition for the field storing the WEP key input state.

  Revision 1.12  2006/09/02 09:07:41  seiki_masashi
  Corrected comment

  Revision 1.11  2006/09/02 05:14:18  seiki_masashi
  Changed the NCDProxyServerProfile structure

  Revision 1.10  2006/08/21 11:16:46  seiki_masashi
  Added NCDProxyServerProfile structure.

  Revision 1.9  2006/08/16 04:01:08  seiki_masashi
  Changed the size of the user name and password for proxy authentication from 32 to 33

  Revision 1.8  2006/08/16 03:38:06  seiki_masashi
  Added a user name and password to the Proxy settings
  Added version information to NCDConfig

  Revision 1.7  2006/08/15 05:58:46  seiki_masashi
  Changed Proxy settings

  Revision 1.6  2006/08/15 05:19:06  terui
  Cleaned up by deleting items that could not be set from the NCDIpAdjustProfile structure

  Revision 1.5  2006/08/14 23:38:24  terui
  Moved the timeWaitBufSize and reassemblyBufSize members to reserved

  Revision 1.4  2006/08/11 01:47:11  terui
  Handled wired LAN fixed link type non-support

  Revision 1.3  2006/08/05 11:54:21  terui
  Support for the specifications added for link UP request timeout
  s64 members changed to s32

  Revision 1.2  2006/07/29 04:18:44  terui
  Added members for enabling NWC24.

  Revision 1.1  2006/07/26 07:24:17  terui
  Moved from posrts/shared

  Revision 1.3  2006/07/24 10:39:50  terui
  Changed adjustment parameter definitions of the socket library.

  Revision 1.2  2006/07/22 11:57:49  terui
  Moved constant definitions to NCDConst.h.
  Removed some internal structures and switched to using typedef.
  Added item for setting the send retry for wireless communications.
  Defined new structures forming the API interface.

  Revision 1.1  2006/06/16 07:33:21  terui
  Moved sc/sctypes.h and changed prefix to NCD.

 *---------------------------------------------------------------------------*/
