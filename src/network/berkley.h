#ifndef BERKLEY_H

#define BERKLEY_H

#include "so.h"

typedef int ssize_t;
typedef unsigned long size_t;

#define CONSOLE_BUS_SPEED ((vu32*)0x800000F8)

typedef unsigned int socklen_t;
typedef struct sockaddr sockaddr;
struct sockaddr
{
	/* TODO */
	sa_family_t sa_family;
	char        sa_data[14];
};

unsigned int htonl(unsigned int h) { return SOHtoNl(h); }
unsigned short htons(unsigned short h) { return SOHtoNs(h); }
unsigned int ntohl(unsigned int n) { return SONtoHl(n); }
unsigned short ntohs(unsigned short n) { return SONtoHs(n; }
int poll(SOPollFD * fd, unsigned int nfds, int timeout)
{
	unsigned int speed = *CONSOLE_BUS_SPEED;
	/* FIXME */
	unsigned long long ticks = timeout * speed;
	SOPoll(fd, nfds, ticks);
}
int bind(int sockfd, sockaddr const * addr, socklen_t addrlen) { return SOBind(sockfd, addr, addrlen); }
int connect(int sockfd, sockaddr const * addr, socklen_t addrlen) { return SOConnect(sockfd, addr); }
ssize_t recv(int sockfd, void * buf, size_t len, int flags) { return SORecv(sockfd, buf, len, flags); }
ssize_t recvfrom(int sockfd, void * buf, size_t len, int flags, sockaddr * addr, socklen_t * addrlen) { return SORecvFrom(sockfd,buf,len,flags,addr,addrlen); }
ssize_t send(int sockfd, void const * buf, size_t len, int flags) { return SOSend(sockfd,buf,len,flags); }
ssize_t sendto(int sockfd, void const * buf, size_t len, int flags, sockaddr const * addr, socklen_t addrlen) { return SOSendTo(sockfd,buf,len,flags,addr,addrlen); }
int setsockopt(int sockfd, int level, int optname, void const * optval, socklen_t optlen) { return SOSetSockOpt(sockfd,level,optname,optval,optlen); }
int socket(int domain, int type, int protocol) { return SOSocket(domain,type,protocol); }
/* FIXME - not berkley naming */
int closesocket(int sockfd) { return SOClose(sockfd); }

/* EXTRAS  - FIXME */
int getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen) { SOGetSockOpt(sockfd,level,optname,optval,optlen); }


#endif /* end of include guard: BERKLEY_H */
