#ifndef BINSTREAM_H

#define BINSTREAM_H

/* TODO Arch specifics */
static inline u16 SWAP16(u16 value)
{
	return ((value & 0xFF00) >> 8) | ((value & 0x00FF) << 8);
}
static inline u32 SWAP32(u16 value)
{
	return ((value & 0xFF000000UL) >> 24) |
		((value & 0x00FF0000UL) >>  8) |
		((value & 0x0000FF00UL) <<  8) |
		((value & 0x000000FFUL) << 24);
}
static inline u64 SWAP64(u64 value)
{
	return ((value & 0xFF00000000000000ULL) >> 56) |
		((value & 0x00FF000000000000ULL) >> 40) |
		((value & 0x0000FF0000000000ULL) >> 24) |
		((value & 0x000000FF00000000ULL) >>  8) |
		((value & 0x00000000FF000000ULL) <<  8) |
		((value & 0x0000000000FF0000ULL) << 24) |
		((value & 0x000000000000FF00ULL) << 40) |
		((value & 0x00000000000000FFULL) << 56);
}

/* FIXME class */
typedef struct Symbol Symbol;
struct Symbol
{
	const char * str;
};

typedef struct String String;
struct String
{
	int foo;
	int bar;
	const char * buf;
};

/* TODO SwapData(void*, void*, int)  */
/* TODO RandomInt(void); */
/* TODO MemAllocTemp(int size, int align) */
/* TODO MemFree(void * buffer) */

class BinStream
{
	public:
		enum SeekType
		{
			SET = 0,
			CURRENT = 1,
			END = 2
		}
		bool swap;
		void * buffer; // related to encryption // class Rand sizeof==4

		BinStream(bool swap);
		virtual ~BinStream(void);
		virtual void Flush(void);
		virtual int Tell(void);
		virtual int Eof(void);
		virtual bool Fail(void);
		virtual const char * Name(void);	//BinStream::Name()
		virtual Cached(void);				//BinStream::Cached()
		virtual GetPlatform(void);			//BinStream::GetPlatform()
		virtual void ReadImpl(void * buffer, int length);
		virtual void WriteImpl(void const * buffer, int length);
		virtual void SeekImpl(int where, BinStream::SeekType type);

		/* non-virtual */
		BinStream& operand<<(BinStream &stream, const char * buf);
		BinStream& operand<<(BinStream &stream, Symbol const & symbol);
		BinStream& operand<<(BinStream &stream, String const & str);
		BinStream& operand>>(BinStream &stream, unsigned int & var);
		BinStream& operand>>(BinStream &stream, Symbol & symbol);
		BinStream& operand>>(BinStream &stream, String & str);
		void EnableReadEncryption(void);
		void EnableWriteEncryption(int foo);
		void DisableEncryption(void);
		void Read(void * buffer, int length);
		void Write(void const * buffer, int length);
		void Seek(int where, BinStream::SeekType whence);
		void Skip(int amount);
		void ReadEndian(void * buffer, int length);
		void WriteEndian(void const * buffer, int length);
};

const char * BinStream::Name(void) { return "<unnamed>"; }
BinStream& operand<<(BinStream &stream, const char * buf)
{
	/* FIXME */
	int len = strlen(buf);
	int _len[1];
	_len[0] = len;
	stream.WriteEndian(_len, 4);
	stream.Write(buf, len);
	return stream;
}
BinStream& operand<<(BinStream &stream, Symbol const & symbol)
{
	/* FIXME */
	const char * buf = symbol.buf;
	int len = strlen(buf);
	int _len[1];
	_len[0] = len;
	stream.WriteEndian(_len, 4);
	stream.Write(buf, len);
	return stream;
}
BinStream& operand<<(BinStream &stream, String const & str)
{
	/* FIXME */
	int len = strlen(str.buf);
	int _len[1];
	_len[0] = len;
	stream.WriteEndian(_len, 4);
	stream.Write(str.buf, len);
	return stream;
}
void BinStream::ReadString(char * str, int length)
{
	unsigned int len;
	this >> len;
	this->Read(str, len);
	*(str + len) = '\x0';
}
BinStream& operand>>(BinStream &stream, unsigned int & var)
{
	stream.Read(var, 4);
	if (stream.swap)
		SwapData(var, var, 4);
	return stream;
}
BinStream& operand>>(BinStream &stream, Symbol & symbol)
{
	/* FIXME */
	char buf[0x200];
	stream.ReadString(buf, 0x200);
	Symbol symb(buf);
	symbol.str = symb;
	return stream;
}
BinStream& operand>>(BinStream &stream, String & str)
{
	/* FIXME */
	unsigned int var;
	stream >> var;
	str.resize(var);
	streamRead(str.buf, var);
	return stream;
}
BinStream::BinStream(bool swap) { this->swap = swap; this->buffer = 0; }
BinStream::~BinStream(void) { delete this->buffer; /* if (del) delete this; */ }
void BinStream::EnableReadEncryption(void)
{
	/* TODO */
	unsigned int var[1];
	this->Read(var,4);
	if (this->swap)
		SwapData(var,var,4);
	Rand2 * rand2 = new Rand2(*var);
	this->buffer = rand2;
}
void BinStream::EnableWriteEncryption(int foo)
{
	if (foo == -1)
		foo = RandomInt();
	unsigned int var[1];
	var[0] = foo;
	this->WriteEndian(var, 4);
	Rand2 * rand2 = new Rand2(foo);
	this->buffer = rand2;
}
void BinStream::DisableEncryption(void) { delete this->buffer; this->buffer = 0; }
void BinStream::Read(void * buffer, int length)
{
	u8 * buf = buffer; 

	if (this->Fail())
	{
		this->Name();
		return;
	}

	this->ReadImpl(buffer, length);
	if (this->buffer)
	{
		u8 * end = buf + length;
		while ( buf < en )
		{
			*buf = *buf ^ this->buffer->Int();
			buf++;
		}
	}
}
void BinStream::Write(void const * buffer, int length)
{
	u8 * buf = buffer; 
	int len[1]; len[0] = length;

	if (this->Fail())
	{
		this->Name();
		return;
	}

	if (!this->buffer)
	{
		return this->WriteImpl(buffer, *len);
	}

	/* FIXME encryption shit */
	return this->WriteImpl(buffer, *len);
}
void BinStream::Seek(int where, BinStream::SeekType whence)
{
	this->Fail();
	this->SeekImpl(where, whence);
}
void BinStream::Skip(int amount)
{
	this->Fail();
	if (amount)
	{
		void * temp = MemAllocTemp(amount, 0);
		this->ReadImpl(temp, amount);
		MemFree(temp);
	}
}
void BinStream::ReadEndian(void * buffer, int length)
{
	this->Read(buffer, length);
	if (this->swap)
		SwapData(buffer, buffer, length);
}
SwapData(void const * buf1, void * buf2, int length)
{
	if (length == 2)
	{
		u16 * _buf1 = (u16*)buf1;
		u16 * _buf2 = (u16*)buf2;
		*_buf2 = SWAP16(*_buf1);
	}
	else if (length == 4)
	{
		u32 * _buf1 = (u32*)buf1;
		u32 * _buf2 = (u32*)buf2;
		*_buf2 = SWAP32(*_buf1);
	}
	else if (length == 8)
	{
		u64 * _buf1 = (u64*)buf1;
		u64 * _buf2 = (u64*)buf2;
		*_buf2 = SWAP64(*_buf1);
	}
}
void BinStream::WriteEndian(void const * buffer, int length)
{
	if (this->swap)
	{
		u8 buf[8];
		SwapData(buffer, buf, length);
		return this->Write(buf, length);
	}
	this->Write(buffer, length);
}

#endif /* end of include guard: BINSTREAM_H */
