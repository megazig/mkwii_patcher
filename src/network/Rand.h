#ifndef RAND_H

#define RAND_H

class Rand
{
	public:
		int index1;
		int index2;
		int rands[0x100];
		float some_float;
		bool checked;

		Rand(int start);
		void Seed(int start);
		int Int(int foo, int bar);
		float Float(float foo, float bar);
		int Int(void);
		float Gaussian(void);
};

Rand::Rand(int start)
{
	this->index1 = 0;
	this->index1 = 0;
#ifndef _HAS_MEMSET		/* MEGAEDIT */
	for (int ii = 0; ii < 0x100; ii++)
	{
		this->rands[ii] = 0;
	}
#else
	memset(this->rands, 0, 0x100);
#endif
	this->checked = false;
	this->Seed(start);
}

void Rand::Seed(int start)
{
	u32 seed = *(u32*)&start;
	for (int ii = 0; ii < 0x100; ii++)
	{
#define BASE_SEED 0x41C64E6DUL
		seed = seed * BASE_SEED;
		seed += 0x3039;
		u32 temp = (seed >> 16) & 0x0000FFFFUL;
		seed = seed * BASE_SEED;
		seed += 0x3039;
		u32 _temp1 = ROL(seed, 0) & 0x7FFF0000UL;
		u32 _temp2 = temp & 0x8000FFFFUL;
		temp = _temp1 | _temp2;

		this->rands[ii] = *(int*)&temp;
	}
	this->index1 = 0;
	this->index2 = 0x67;
}

int Rand::Int(int _min, int _max)
{
	int baz = this->Int();
	int diff = _max - _min;
	int r = baz / diff;
	r = r * diff;
	r = baz - r;
	return _min + r;
}

float Rand::Float(float _min, float _max)
{
	/* TODO */
	return 0.0;
}

int Rand::Int(void)
{
	int foo = this->rands[this->index1];
	int bar = this->rands[this->index2];
	int value = foo ^ bar;
	this->rands[this->index1] = value;
	this->index1++;
	if (this->index1 >= 0xF9)
		this->index1 = 0;
	this->index2++;
	if (this->index2 >= 0xF9)
		this->index2 = 0;

	return value;
}

float Rand::Gaussian(void)
{
	/* TODO */
	return 0.0;
}

Rand gRand(0x29A);
void SeedRand(int start)
{
	gRand.Seed(start);
}
int RandomInt(void)
{
	return gRand.Int();
}
int RandomInt(int _min, int _max)
{
	int baz = gRand.Int();
	int diff = _max - _min;
	int r = baz / diff;
	r = r * bla;
	r = baz - r;
	return foo + r;
}

float RandFloat(void)
{
	/* TODO */
	return 0.0;
}

float RandFloat(float _min, float _max)
{
	gRand.Float(_min,_max);
}

/* __sinit__Rand_cpp(void) { } */

class Rand2
{
	public:
		int value;

		Rand2(int value);
		int Int(void);
};

Rand2::Rand2(int value)
{
	this->value = value;
	if (!value)
		this->value = 1;
	if (value >= 0)
		return;
	this->value = -value;
}

int Rand2::Int(void)
{
#define RAND2_BASE 0x1F31D
	int foo = this->value / RAND2_BASE;
	int bar = foo * 0xB14;
	foo = foo * RAND2_BASE;
	foo = this->value - foo;
	foo = foo * 0x41A7;
	bar = foo - bar;
	if (bar > 0)
		this->value = bar;
	else
		this->value = bar - 0x80000000 - 1;
	return this->value;
}

/* MEGA ADD */
void SeedRand(void)
{
	int tick;
	inline asm { "mftb %0" : r=(tick) : };
	gRand.Seed(tick);
}

#endif /* end of include guard: RAND_H */
