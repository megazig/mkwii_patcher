#ifndef NETWORKSOCKET_WII_H

#define NETWORKSOCKET_WII_H

class WiiNetworkSocket
{
	public:
		int socket;
		bool _field_08;
		bool fail;

		WiiNetworkSocket(bool foo);
		WiiNetworkSocket(int socket, bool foo);
		//~WiiNetworkSocket(int del);
		virtual ~WiiNetworkSocket(void);
		virtual bool Connect(unsigned int r4, unsigned short r5);
		virtual bool Fail(void);
		virtual void Disconnect(void);
		virtual void Bind(unsigned short r4);
		virtual int  InqBoundPort(unsigned short & r4);
		virtual void Listen(void);
		virtual WiiNetworkSocket * Accept(void);
		virtual void GetRemoteIP(unsigned int & r4, unsigned short & r5);
		virtual int CanSend(void);
		virtual bool BytesAvailable(void);
		virtual int Send(void const * buffer, unsigned long size);
		virtual int Recv(void * buffer, unsigned long size);
		virtual int SendTo(void const * buffer, unsigned long size, unsigned int r6, unsigned short r7);
		virtual int BroadcastTo(void const * buffer, unsigned long size, unsigned short r6);
		virtual int RecvFrom(void * buffer, unsigned long size, unsigned int & r6, unsigned short & r7);
		virtual bool SetNoDelay(bool delay);

		bool Init(void);
		void Clean(void);

		//static bool WiiNetworkSocket::sInit;
		static bool sInit;
};

#endif /* end of include guard: NETWORKSOCKET_WII_H */
