#include "fa.h"
#include "pf.h"

const char * FAVer = "<< RVL_SDK - FA \trelease build: Jul  5 2007 17:57:31 (0x4199_60831) >>";
int FAProfileInitialized = 0;
int FAInInitializing = 0;
int FADiskInitialized = 0;

// TODO - finish
int FAAttach(int r3, int r4, int r5, int r6)
{
	if (r3 < 4)
	{
		if (r6)
		{
		}
		else
		{
			return -2;
		}
	}
	else
	{
		return -2;
	}
}

int FABuffering(int r3, int r4)
{
	return (pfstub_buffering(r3, r4) != 0);
}

int FACreate(const char * filename, int mode)
{
	return pfstub_create(filename, mode);
}

int FACreatedir(int r3, int r4, int r5)
{
	return (pfstub_createdir(r3, r4, r5) != 0);
}

int FADetach(char drive)
{
	char which = drv - 'A';
	int partition = gOpenPartition[which];
	int disk      = gOpenDisk[which];

	int ret = pfstub_detach(drive);
	if (ret)
		return -1;

	if (!disk) return 0;
	if (!partition) return 0;

	ret = pdm_close_partition(partition);
	if (!ret)
		return -1;

	ret = pdm_close_disk(disk);
	if (!ret)
		return -1;

	return 0;
}

int FAErrnum(void)
{
	int err = pfstub_errnum();
	if (err == 5)
		return 5;
	// TODO - fix me
	return err == 0;
}

int FAFclose(int fd)
{
	return (pfstub_fclose(fd) != 0);
}

int FAFinfo(int r3, int r4)
{
	return (pfstub_finfo(r3, r4) != 0);
}

int FAFopen(const char * filename, const char * mode)
{
	return pfstub_fopen(filename, mode);
}

int FAFormat(int r3, int r4)
{
	return (pfstub_format(r3, r4) != 0);
}

int FAFread(void* buffer, int size, int count, int fd)
{
	return pfstub_read(buffer, size, count, fd);
}

int FAFseek(int fd, int where, int whence)
{
	return (pfstub_fseek(fd, where, whence) != 0);
}

int FAFsfirst(int r3, int r4, int r5)
{
	return (pfstub_fsfirst(r3, r4, r5) != 0);
}

int FAFsnext(int r3)
{
	return (pfstub_fsnext(r3) != 0);
}

// TODO - fix stat struct
int FAFstat(const char * filename, void * stat)
{
	return (pfstub_fstat(filename, stat) != 0);
}

int FAFwrite(void* buffer, int size, int count, int fd)
{
	return pfstub_fwrite(buffer, size, count, fd);
}

int FAGetdev(char r3, int r4)
{
	return (pfstub_devinf(r3, r4) != 0);
}

int FAInit(int r3)
{
	if (FAInInitializing == 1)
		return -1;

	FAInInitializing = 1;
	if (!FADiskInitialized)
	{
		int ret = pdm_init_diskmanager(0,0);
		FADiskInitialized = (ret != 0);
		if (ret != 0)
		{
			FAInInitializing = 0;
			return -1;
		}
	}

	if (!FAProfileInitialized)
	{
		int ret = pfstub_init_prfile2(r3, 0);
		FAProfileInitialized = (ret != 0);
		if (ret == -1)
		{
			FAInInitializing = 0;
			return -1;
		}
	}

	OSRegisterVersion(FAVer);
	FAInInitializing = 0;
	return 0;
}

int FAMount(char drive)
{
	return (pfstub_mount(drive) != 0);
}

#define FA_TYPE_USB 1
#define FA_TYPE_SD  0
#define FA_ERR_BAD_TYPE -2
int gOpenDisk[26];
int gOpenPartition[26];
// TODO - fixme
//void (*)(char) gMediaInOutCallback[4];
void (*gMediaInOutCallback)(char);
void (*gMediaInOutCallback2)(char);
void (*gMediaInOutCallback3)(char);
void (*gMediaInOutCallback4)(char);
// TODO - fixup cb defs
int FARegistCB(int type, void * cb1, void * cb2)
{
	int ret;
	if (type == FA_TYPE_USB)
	{
		gMediaInOutCallback3 = cb1;
		gMediaInOutCallback4 = cb2;
		ret = pfd_mscdrv_register_callback(FANotifyUSBInsert, FANotifyUSBEject);
	}
	else if (type == FA_TYPE_SD)
	{
		gMediaInOutCallback  = cb1;
		gMediaInOutCallback1 = cb2;
		ret = pfd_sddrv_register_callback(FANotifySDInsert, FANotifySDEject);
	}
	else
	{
		return -2;
	}

	if (ret)
		return -1;
	return 0;
}

void FANotifySDInsert(char drive)
{
	if (!gMediaInOutCallback)
		return;
	gMediaInOutCallback(drive);
}

int FANotifySDEject(char drive)
{
	if (!gMediaInOutCallback2)
		return;
	gMediaInOutCallback2(drive);
}

void FANotifyUSBInsert(char drive)
{
	if (!gMediaInOutCallback3)
		return;
	gMediaInOutCallback3(drive);
}

int FANotifyUSBEject(char drive)
{
	if (!gMediaInOutCallback4)
		return;
	gMediaInOutCallback4(drive);
}

int FARemove(const char * filename)
{
	return (pfstub_remove(filename) != 0);
}

int FASetThreadPriority(int priority)
{
	int ret;
	ret = pfstub_set_stub_priority(priority);
	if (!ret)
	{
		return (usbh_msc_set_thread_priority(priority) != 0);
	}

	// TODO - what?
	return (ret == -2);
}

int FAUnmount(char drive, int mode)
{
	int ret = pfstub_unmount(drive, mode);
	if (ret == -1)
		return -1;

	if (ret == 1)
		return 1;

	return (ret != 0);
}

